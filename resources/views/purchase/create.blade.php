@extends('layouts.default')

@section('css')
    <style>
        td .custom-control{
            margin-top: 0.5rem;
        }
    </style>
@endsection

@section('content')
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('purchases.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Tambah Pengadaan</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
          <div class="breadcrumb-item"><a href="#">Pengadaan</a></div>
          <div class="breadcrumb-item">Tambah Pengadaan</div>
        </div>
      </div>

      <form class="form" action="{{ route('purchases.store') }}" method="POST" id="form-product">
        @csrf
      <div class="section-body">
        <div class="row mt-4">
          <template v-if="productions.length">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4>Data Produksi</h4>
              </div>
              <div class="card-body">
                <div class="card-body">
                    <h2 class="section-title">
                        Pilih Data Produksi
                    </h2>
                    @error('production_ids')
                    <div class="text-danger text-small">
                        {{ $message }}
                    </div>
                    @enderror
                    <div class="table-responsive">
                      <table class="table table-bordered table-md">
                        <tr>
                          <th>
                            <div class="custom-checkbox custom-control">
                              <input type="checkbox" class="custom-control-input" id="checkbox-all" v-model="select_all" @change="toggleSelectAll">
                              <label for="checkbox-all" class="custom-control-label">&nbsp;</label>
                            </div>
                          </th>
                          <th>Kode Produksi</th>
                          <th>Tanggal Produksi</th>
                          <th>#</th>
                        </tr>
                        <tr v-for="production, index in productions" :key="index">
                          <td class="p-0 text-center">
                            <div class="custom-checkbox custom-control">
                              <input type="checkbox" class="custom-control-input" v-model="production.selected" :value="production.id" :id="'checkbox-'+index" name="production_ids[]">
                              <label :for="'checkbox-'+index" class="custom-control-label">&nbsp;</label>
                            </div>
                          </td>
                          <td><b>@{{ production.code }}</b></td>
                          <td>@{{ production.tanggal_produksi }}</td>
                          <td><a :href="'{{route('productions.index')}}/'+production.id" target="_blank" class="btn btn-sm btn-info"><span class="fa fa-eye"></span></a></td>
                        </tr>
                      </table>
                    </div>

                    <h2 class="section-title">
                        Jumlah Produksi
                    </h2>
                    <table class="table form-table">
                        <thead>
                            <tr>
                                <td class="form-group"><label>No</label></td>
                                <td class="form-group"><label>Produk</label></td>
                                <td class="form-group"><label>Jumlah</label></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="item, key in summary_productions" :key="key">
                                <td>@{{ key+1 }}</td>
                                <td>@{{ item.product_name }}</td>
                                <td>@{{ item.total_size }} Liter</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="card">
              <div class="card-header">
                <h4>Detail Pengadaan</h4>
                <div class="card-header-action">
                    <button type="submit" class="btn btn-primary float-right">Simpan</button>
                </div>
              </div>
              <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table form-table">
                                <thead>
                                    <tr>
                                        <td class="form-group"><label>Material</label></td>
                                        <td class="form-group"><label>Stock</label></td>
                                        <td class="form-group"><label>Kebutuhan</label></td>
                                        <td class="form-group"><label>Supplier</label></td>
                                        <td class="form-group" width="90"><label>Pembelian</label></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="purchase, key in purchases" :key="key" v-show="purchase.production_need > 0">
                                        <td>
                                            @{{ purchase.material.name }}
                                        </td>
                                        <td>
                                            <input type="hidden" :value="purchase.material.stock" :name="'purchases['+key+'][from_stock]'">
                                            @{{ purchase.material.stock }} @{{ purchase.material.unit_string }}
                                        </td>
                                        <td>
                                            <input type="hidden" :value="Math.ceil(purchase.production_need)" :name="'purchases['+key+'][production_need]'" :id="'production_need'+key">
                                            @{{ purchase.production_need }} @{{ purchase.material.unit_string }}
                                        </td>
                                        <td>
                                            <select class="form-control" :name="'purchases['+key+'][material_price_id]'" @change="getMinQuantity(key)" :id="'material'+key">
                                                <option :value="material.id+'_'+material.min_order" v-for="material, index in purchase.material.prices" :key="index">
                                                    @{{ material.supplier.name }} (@{{ material.rupiah }}) (Min: @{{material.min_order }} @{{ purchase.material.unit_string }})
                                                </option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="number" class="form-control" :value="Math.ceil(purchase.min_order)" :min="Math.ceil(purchase.min_order)" :name="'purchases['+key+'][quantity]'" :id="'quantity'+key">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
          </div>
          </template>
          <template v-else>
              <div class="col-md-12">
                <center>
                <div class="empty-state" data-height="400" style="height: 400px;">
                    <div class="empty-state-icon">
                    <i class="fas fa-question"></i>
                    </div>
                    <h2>Tidak ada permintaan pengadaan</h2>
                    <p class="lead">
                    Silahkan buat rencana produksi terlebih dahulu.
                    </p>
                </div>
                </center>
            </div>
          </template>
        </div>
      </div>
      </form>
    </section>
  </div>
@endsection

@section('js')
<script src="{{ asset("/js/vue-dev.js") }}"></script>
<script>
    function submit() {
        $('#form-product').submit()
    }

    var app = new Vue({
        el: '#app',
        data: {
            productions: {!! json_encode($productions) !!},
            products: {!! json_encode($products) !!},
            summary_productions: [],
            purchases:[],
            select_all:true
        },
        watch: {
            selectedProduction(){
                if(this.productions.length == this.selectedProduction.length){
                    this.select_all = true
                }else{
                    this.select_all = false
                }

                this.generateDetailPurchases()
            }
        },
        methods: {
            toggleSelectAll(){
                this.productions.forEach(production => {
                    production.selected = this.select_all
                });
            },
            generateDetailPurchases(){
                let selectedProd = JSON.parse(JSON.stringify(this.selectedProduction))

                // this.summary_productions = []
                this.summary_productions = Object.values(Object.values(selectedProd.map((production) => {
                        return production.details
                    }).flat(1)
                    .reduce((result, production) => {
                        if (typeof result[production.product_variant_id] === 'undefined') {
                            result[production.product_variant_id] = production
                        }else{
                            result[production.product_variant_id]['quantity'] += production.quantity
                        }
                        return result
                    }, {})).map((production) => {
                        let total_size = parseFloat((production.product_size * production.quantity).toFixed(2))
                        return {
                            product_id: production.product_variant.product_id,
                            product_name: production.product_name,
                            size: production.product_size,
                            quantity: production.quantity,
                            total_size: total_size,
                        }
                    }).reduce((result, production) => {
                        if (typeof result[production.product_id] === 'undefined') {
                            result[production.product_id] = production
                        }else{
                            result[production.product_id]['product_id'] = production.product_id
                            result[production.product_id]['product_name'] = production.product_name
                            result[production.product_id]['size'] += production.size
                            result[production.product_id]['quantity'] += production.quantity
                            result[production.product_id]['total_size'] += production.total_size
                        }
                        return result
                    }, {})
                )

                this.products.forEach(product => {
                    let production = this.summary_productions.find(production => production.product_id == product.id)
                    product.boms.map(bom => {
                        if(production){
                            bom.production_need = parseFloat((production.total_size * bom.dose).toFixed(2))
                            bom.min_order = Math.ceil(bom.material.prices[0].min_order >= bom.production_need ? bom.material.prices[0].min_order : bom.production_need)
                        }else{
                            bom.production_need = 0
                            bom.min_order = Math.ceil(bom.material.prices[0].min_order > bom.production_need ? bom.material.prices[0].min_order : bom.production_need)
                        }
                    })
                });

                this.purchases = []
                this.purchases = Object.values(
                    this.products.map(product => {
                        return product.boms
                    }).flat(1).filter(bom => {
                        return bom.production_need > 0
                    }).reduce((result, bom) => {
                        if (typeof result[bom.material_id] === 'undefined') {
                            result[bom.material_id] = bom
                            result[bom.material_id]['production_need'] = parseFloat(bom.production_need.toFixed(2))
                            result[bom.material_id]['min_order'] = bom.min_order
                        }else{
                            result[bom.material_id]['material_id'] = bom.material_id
                            result[bom.material_id]['production_need'] += bom.production_need
                        }
                        return result
                    }, {})
                ).map(purchase => {
                    purchase.production_need = parseFloat(purchase.production_need.toFixed(2)) - parseFloat(purchase.material.stock)
                    if(purchase.production_need > purchase.min_order){
                        purchase.min_order = purchase.production_need
                    }
                    return purchase
                })
            },
            getMinQuantity(key){
                quantity = document.getElementById('quantity'+key)
                production_need = document.getElementById('production_need'+key)
                material = document.getElementById('material'+key)

                if(production_need.value > parseInt(material.value.split('_')[1])){
                    quantity.value = production_need.value
                }else{
                    quantity.value = parseInt(material.value.split('_')[1])
                }

                quantity.setAttribute("min", quantity.value);
            }
        },
        computed:{
            selectedProduction(){
                return this.productions.filter(production=>{
                    return production.selected
                })
            }
        },
        created(){
            this.generateDetailPurchases()
        }
    })
</script>
@endsection
