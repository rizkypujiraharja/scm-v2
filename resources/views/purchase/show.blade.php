@extends('layouts.default')

@section('content')
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('purchases.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Detail Pengadaan</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
          <div class="breadcrumb-item"><a href="{{ route('purchases.index') }}">Pengadaan</a></div>
          <div class="breadcrumb-item">Detail Pengadaan</div>
        </div>
      </div>
      <div class="section-body">
        <div class="row mt-4">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                  <h4>Data Pengadaan : {{$purchase->code }}</h4>
                  <div class="card-header-action">
                    <a href="{{ route('purchases.show.report', $purchase) }}" class="btn btn-success mr-1">
                        Cetak
                    </a>
                    @switch($purchase->status)
                        @case(0)
                            <a href="{{ route('purchases.toggle_status', $purchase) }}?status=1" class="btn btn-primary">
                                Terima
                            </a>
                            @break
                    @endswitch
                  </div>
                </div>
                <div class="card-body">
                  <div class="row mb-2">
                    <div class="col-md-5">
                      <b>{{ $purchase->supplier->name }}</b><br>
                      {{ $purchase->supplier->address }}<br>
                      {{ $purchase->supplier->phone }}
                    </div>
                    <div class="col-md-3">
                      <b>Status Pengadaan</b><br>
                      {!! $purchase->label_status !!}
                    </div>
                  </div>
                </div>
            </div>

            <div class="card">
              <div class="card-header">
                <h4>Detail Pengadaan</h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-striped">
                    <tr>
                      <th>No</th>
                      <th>Nama Bahan Baku</th>
                      <th>Jumlah</th>
                      <th>Harga</th>
                      <th>Sub Total</th>
                    </tr>
                    @forelse ($purchase->details as $key=>$item)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $item->materialPrice->material->name }}</td>
                        <td>{{ $item->quantity }} {{ $item->materialPrice->material->unit_string }}</td>
                        <td>{{ $item->harga}}</td>
                        <td>{{ $item->sub_total_harga}}</td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="4" align="center">
                            <div class="empty-state" data-height="400" style="height: 400px;">
                                <div class="empty-state-icon">
                                  <i class="fas fa-question"></i>
                                </div>
                                <h2>Data Tidak Ditemukan</h2>
                                <p class="lead">
                                  Maaf kami tidak dapat menemukan data.
                                </p>
                            </div>
                        </td>
                    </tr>
                    @endforelse
                  </table>
                </div>
              </div>
            </div>
        </div>
      </div>
    </section>
  </div>
@endsection

@section('js')

@endsection
