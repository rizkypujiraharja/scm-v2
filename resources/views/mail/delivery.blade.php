<!DOCTYPE html>
<html>
<head>
  <title>Report Order</title>
  <style type="text/css">
    .table {
        width: 100%;
        margin-bottom: 1rem;
        background-color: transparent;
        border-collapse: collapse;
    }

    .table-bordered th, .table-bordered td{
      border: 1px solid #dee2e6;
    }

    .table th, .table td{
      padding: 3px;
    }

    .align-center th, .align-center td{
      text-align: center;;
    }

    .align-left th, .align-left td{
      text-align: left;;
    }

    h3, h4{
      color: #a23234;
    }

    .text-primary{
      color: #a23234;
    }

  </style>
</head>
<body>
  Hai, {{$order->customer_name}}<br>
  Pesanan <strong class="text-primary">{{ $order->invoice_code }}</strong> telah dikirim pada {{ $date }}
  <hr>
  <h3>Kode Pesanan {{ $order->invoice_code }}</h3>
  <table class="table align-left">
    <tr>
        <th width="30%">Nama Pelanggan</th>
        <td width="70%">{{ $order->customer_name }}</td>
    </tr>
    <tr>
        <th>No Telepon</th>
        <td>{{ $order->customer_phone }}</td>
    </tr>
    <tr>
        <th>Email</th>
        <td>{{ $order->customer_email }}</td>
    </tr>
    <tr>
        <th>Alamat</th>
        <td>{{ $order->customer_address }}</td>
    </tr>
    <tr>
        <th>Estimasi Waktu</th>
        <td>{{ $order->estimated_days }} Hari</td>
    </tr>
    <tr>
        <th>Total Harga</th>
        <td>{{ $order->total_harga }}</td>
    </tr>
    <tr>
        <th>Tipe</th>
        <td>{!! $order->label_type !!}</td>
    </tr>
    <tr>
        <th>Tanggal Pesan</th>
        <td>{{ $order->tanggal_pesan }}</td>
    </tr>
  </table>
  <h4>Data Produk</h4>
  <table class="table table-bordered align-center">
      <thead>
          <tr>
              <th>No</th>
              <th>Nama Produk</th>
              <th>Ukuran Produk</th>
              <th>Qty</th>
              <th>Harga</th>
              <th>Sub Total</th>
          </tr>
      </thead>
      <tbody>
      @php $no = 1 @endphp
      @foreach ($products as $product => $details)
          @foreach ($details as $key => $item)
          <tr>
              @if ($key == 0)
              <td rowspan="{{ count($details) }}">{{ $no }}</td>
              <td rowspan="{{ count($details) }}">{{ $item->product_name }}</td>
              @endif
              <td>{{ $item->product_size }} Liter</td>
              <td>{{ $item->quantity }}</td>
              <td>{{ $item->harga}}</td>
              <td>{{ $item->sub_total_harga}}</td>
          </tr>
          @endforeach
          @php $no++ @endphp
      @endforeach
      </tbody>
  </table>

</body>
</html>