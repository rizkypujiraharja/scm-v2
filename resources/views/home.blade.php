@extends('layouts.default')
@section('css')
<style type="text/css">
    .card-recent {
      height: 250px;
    }
</style>
@endsection
@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Dashboard</h1>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                      <div class="card card-statistic-1">
                        <div class="card-icon bg-primary">
                          <i class="fas fa-shopping-cart"></i>
                        </div>
                        <div class="card-wrap">
                          <div class="card-header">
                            <h4>Total Pesanan</h4>
                          </div>
                          <div class="card-body">
                             @{{ widget.order.total }}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                      <div class="card card-statistic-1">
                        <div class="card-icon bg-info">
                          <i class="fas fa-address-card"></i>
                        </div>
                        <div class="card-wrap">
                          <div class="card-header">
                            <h4>Total Supplier</h4>
                          </div>
                          <div class="card-body">
                             @{{ widget.supplier.total }}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                      <div class="card card-statistic-1">
                        <div class="card-icon bg-warning">
                          <i class="fas fa-spray-can"></i>
                        </div>
                        <div class="card-wrap">
                          <div class="card-header">
                            <h4>Total Produk</h4>
                          </div>
                          <div class="card-body">
                             @{{ widget.product.total }}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                      <div class="card card-statistic-1">
                        <div class="card-icon bg-success">
                          <i class="fas fa-user"></i>
                        </div>
                        <div class="card-wrap">
                          <div class="card-header">
                            <h4>Total Customer</h4>
                          </div>
                          <div class="card-body">
                             @{{ widget.customer.total }}
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                      <div class="card">
                        <div class="card-header">
                          <h4>Grafik Pesanan</h4>
                        </div>
                        <div class="card-body">
                          <div id="myChart" height="158">
                            <div class="empty-state" data-height="400" style="height: 400px;" v-if="chart_order.total == 0">
                                <div class="empty-state-icon">
                                  <i class="fas fa-question"></i>
                                </div>
                                <h2>Data Tidak Ditemukan</h2>
                                <p class="lead">
                                  Maaf kami tidak dapat menemukan data.
                                </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="card">
                        <div class="card-header">
                          <h4>Grafik Produksi</h4>
                        </div>
                        <div class="card-body">
                          <div id="myChart2" height="158">
                            <div class="empty-state" data-height="400" style="height: 400px;"  v-if="chart_production.total == 0">
                                <div class="empty-state-icon">
                                  <i class="fas fa-question"></i>
                                </div>
                                <h2>Data Tidak Ditemukan</h2>
                                <p class="lead">
                                  Maaf kami tidak dapat menemukan data.
                                </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                      <div class="card">
                        <div class="card-header">
                          <h4>Pesanan Terbaru</h4>
                          <div class="card-header-action">
                            <a href="{{route('orders.index')}}" class="btn btn-info">Lihat lebih banyak <i class="fas fa-chevron-right"></i></a>
                          </div>
                        </div>
                        <div class="card-body p-0 card-recent">
                            <div class="table-responsive table-invoice" v-if="recent_order.length != 0">
                                <table class="table table-md table-striped">
                                  <tr>
                                    <th>Kode Pesanan</th>
                                    <th>Tanggal Pesan</th>
                                  </tr>
                                  <tr v-for="item, key in recent_order">
                                    <td><a :href="'{{url('orders')}}/'+item.id">@{{ item.invoice_code }}</a></td>
                                    <td>@{{ item.tanggal_pesan }}</td>
                                  </tr>
                                </table>
                            </div>
                            <div class="empty-state" data-height="200" style="height: 200px;" v-if="recent_order.length == 0">
                                <div class="empty-state-icon">
                                  <i class="fas fa-question"></i>
                                </div>
                                <h2>Data Tidak Ditemukan</h2>
                                <p class="lead">
                                  Maaf kami tidak dapat menemukan data.
                                </p>
                            </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="card">
                        <div class="card-header">
                          <h4>Produksi Terbaru</h4>
                          <div class="card-header-action">
                            <a href="{{route('productions.index')}}" class="btn btn-info">Lihat lebih banyak <i class="fas fa-chevron-right"></i></a>
                          </div>
                        </div>
                        <div class="card-body p-0 card-recent"> 
                            <div class="table-responsive table-invoice" v-if="recent_production.length != 0">
                                <table class="table table-md table-striped">
                                  <tr>
                                    <th>Kode Produksi</th>
                                    <th>Tanggal Produksi</th>
                                  </tr>
                                  <tr v-for="item, key in recent_production">
                                    <td><a :href="'{{url('productions')}}/'+item.id">@{{ item.code }}</a></td>
                                    <td>@{{ item.tanggal_produksi }}</td>
                                  </tr>
                                </table>
                            </div>
                            <div class="empty-state" data-height="200" style="height: 200px;" v-if="recent_production.length == 0">
                                <div class="empty-state-icon">
                                  <i class="fas fa-question"></i>
                                </div>
                                <h2>Data Tidak Ditemukan</h2>
                                <p class="lead">
                                  Maaf kami tidak dapat menemukan data.
                                </p>
                            </div>
                        </div>
                        
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="card">
                        <div class="card-header">
                          <h4>Pengadaan Terbaru</h4>
                          <div class="card-header-action">
                            <a href="{{route('purchases.index')}}" class="btn btn-info">Lihat lebih banyak <i class="fas fa-chevron-right"></i></a>
                          </div>
                        </div>
                        <div class="card-body p-0 card-recent">
                            <div class="table-responsive table-invoice" v-if="recent_purchase.length != 0">
                                <table class="table table-md table-striped">
                                  <tr>
                                    <th>Kode Pengadaan</th>
                                    <th>Tanggal Pengadaan</th>
                                  </tr>
                                  <tr v-for="item, key in recent_purchase">
                                    <td><a :href="'{{url('purchases')}}/'+item.id">@{{ item.code }}</a></td>
                                    <td>@{{ item.tanggal_pengadaan }}</td>
                                  </tr>
                                </table>
                            </div>
                            <div class="empty-state" data-height="200" style="height: 200px;" v-if="recent_purchase.length == 0">
                                <div class="empty-state-icon">
                                  <i class="fas fa-question"></i>
                                </div>
                                <h2>Data Tidak Ditemukan</h2>
                                <p class="lead">
                                  Maaf kami tidak dapat menemukan data.
                                </p>
                            </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('js')
<script src="{{ asset("/js/vue-dev.js") }}"></script>
<script src="{{ asset("/js/axios.min.js") }}"></script>
<script src="{{ asset("/js/highcharts.js") }}"></script>
<script type="text/javascript">
    var app = new Vue({
        el: '#app',
        data: {
            widget:{
                order:{
                    total:0,
                    data:[]
                },
                supplier:{
                    total:0,
                },
                product:{
                    total:0,
                },
                product:{
                    total:0,
                },
            },
            chart_order:{},
            chart_production:{},
            recent_order:[],
            recent_purchase:[],
            recent_production:[],
        },
        methods: {
            getDataWidget()
            {
                 axios.get('{{ url("/") }}/api/widget', {})
                    .then(response => {
                        console.log(response.data)
                        this.widget = response.data
                    }).catch(error => {
                    });
            },
            loadChartOrder()
            {
                axios.get('{{ url("/") }}/api/chart-order', {})
                    .then(response => {
                        this.chart_order = response.data
                        if(this.chart_order.total){
                            this.drawChart('myChart', this.chart_order)
                        }
                    }).catch(error => {
                        console.log(error)
                    });
            },
            loadChartProduction()
            {
                axios.get('{{ url("/") }}/api/chart-production', {})
                    .then(response => {
                        this.chart_production = response.data
                        if(this.chart_production.total){
                            this.drawChart('myChart2', this.chart_production)
                        }
                    }).catch(error => {
                        console.log(error)
                    });
            },
            drawChart(component, data)
            {
                Highcharts.chart(component, {
                    title: {
                        text: ''
                    },

                    yAxis: {
                        title: {
                            text: ''
                        }
                    },
                    xAxis: {
                        categories: data.categories
                    },

                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },

                    plotOptions: {
                        series: {
                            cursor: 'pointer',
                        }
                    },

                    series:data.chart,

                    responsive: {
                        rules: [{
                            condition: {
                                // maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    },
                    credits: {
                        enabled: false
                    },

                });
            },
            getRecentOrder()
            {
                 axios.get('{{ url("/") }}/api/recent-order', {})
                    .then(response => {
                        this.recent_order = response.data
                    }).catch(error => {
                    });
            },
            getRecentPurchase()
            {
                 axios.get('{{ url("/") }}/api/recent-purchase', {})
                    .then(response => {
                        this.recent_purchase = response.data
                    }).catch(error => {
                    });
            },
            getRecentProduction()
            {
                 axios.get('{{ url("/") }}/api/recent-production', {})
                    .then(response => {
                        this.recent_production = response.data
                    }).catch(error => {
                    });
            },
        },
        created() {
            this.getDataWidget()
            this.loadChartOrder()
            this.loadChartProduction()
            this.getRecentOrder()
            this.getRecentPurchase()
            this.getRecentProduction()
        },
        
    })
</script>
@endsection
