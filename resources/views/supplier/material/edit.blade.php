@extends('layouts.default')

@section('content')
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Edit Bahan Baku</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
          <div class="breadcrumb-item"><a href="#">Supplier</a></div>
          <div class="breadcrumb-item"><a href="#">Bahan Baku</a></div>
          <div class="breadcrumb-item"><a href="#">Edit Bahan Baku</a></div>
        </div>
      </div>
      <div class="section-body">
        <div class="row mt-4">
          <div class="col-12">

            <div class="card">
                <div class="card-header">
                  <h4>Supplier</h4>
                </div>
                <div class="card-body">
                    <table class="table">
                        <tr>
                            <td>{{ $supplier->name }}</td>
                            <td>{{ $supplier->phone }}</td>
                            <td>{{ $supplier->address }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="card">
              <div class="card-header">
                <h4>Harga Bahan Baku</h4>
              </div>
              <div class="card-body">
                <form class="form" action="{{ route('suppliers.materials.update', [$supplier, $material]) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group row align-items-center">
                        <label for="site-title" class="form-control-label col-sm-3 text-md-right">Bahan Baku</label>
                        <div class="col-sm-6 col-md-9 col-lg-6">
                            <select name="material" class="@error('material') is-invalid @enderror form-control select2" disabled>
                                <option value="{{ $material->material->id }}">{{ $material->material->name .' ('.$material->material->unitString.')' }}</option>
                            </select>
                            @error('material')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row align-items-center">
                        <label for="site-title" class="form-control-label col-sm-3 text-md-right">Harga</label>
                        <div class="col-sm-6 col-md-9 col-lg-6">
                            <input type="number" value="{{ old('price', $material->price) }}" name="price" class="@error('price') is-invalid @enderror form-control">
                            @error('price')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row align-items-center">
                        <label for="site-title" class="form-control-label col-sm-3 text-md-right">Minimal Pembelian</label>
                        <div class="col-sm-6 col-md-9 col-lg-6">
                            <input type="number" value="{{ old('min_order', $material->min_order) }}" name="min_order" class="@error('min_order') is-invalid @enderror form-control">
                            @error('min_order')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row align-items-center">
                        <label for="site-title" class="form-control-label col-sm-3 text-md-right"></label>
                        <div class="col-sm-6 col-md-9 col-lg-6">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection
