@extends('layouts.default')

@section('content')
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Bahan Baku</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
          <div class="breadcrumb-item"><a href="#">Supplier</a></div>
          <div class="breadcrumb-item"><a href="#">Bahan Baku</a></div>
        </div>
      </div>
      <div class="section-body">
        <div class="row mt-4">
          <div class="col-12">

            <div class="card">
                <div class="card-header">
                  <h4>Supplier</h4>
                </div>
                <div class="card-body">
                    <table class="table">
                        <tr>
                            <td>{{ $supplier->name }}</td>
                            <td>{{ $supplier->phone }}</td>
                            <td>{{ $supplier->address }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="card">
              <div class="card-header">
                <h4>Daftar Harga Bahan Baku</h4>
              </div>
              <div class="card-body">
                <div class="float-left">
                    <a href="{{ route('suppliers.materials.create', $supplier) }}" class="btn btn-primary">Tambah Bahan Baku</a>
                </div>
                <div class="float-right">
                  <form action="" method="GET">
                    <div class="input-group">
                      <input type="text" class="form-control" name="search" placeholder="Search">
                      <div class="input-group-append">
                        <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                      </div>
                    </div>
                  </form>
                </div>

                <div class="clearfix mb-3"></div>

                <div class="table-responsive">
                  <table class="table table-striped">
                    <tr>
                      <th>No</th>
                      <th>Bahan Baku</th>
                      <th>Minimal Pembelian</th>
                      <th>Harga</th>
                      <th>#</th>
                    </tr>
                    @forelse ($materials as $item)
                    <tr>
                        <td>{{ (($materials->currentPage()-1) * $materials->perPage()) + $loop->iteration }}</td>
                        <td>{{ $item->material->name }}</td>
                        <td>{{ $item->min_order }} {{ $item->material->unitString }}</td>
                        <td>{{ $item->price }} / ({{ $item->material->unitString }})</td>
                        <td>
                            <a href="{{ route('suppliers.materials.edit', [$supplier, $item]) }}" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="Edit Bahan Baku Supplier"> <span class="fa fa-edit"></span></a>
                            <span data-title="{{ $item->material->name }}" href="{{ route('suppliers.materials.delete', [$supplier, $item]) }}" class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip" data-placement="top" title="Hapus Bahan Baku Supplier"> <span class="fa fa-trash"></span></span>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="4" align="center">
                            <div class="empty-state" data-height="400" style="height: 400px;">
                                <div class="empty-state-icon">
                                  <i class="fas fa-question"></i>
                                </div>
                                <h2>Data Tidak Ditemukan</h2>
                                <p class="lead">
                                  Maaf kami tidak dapat menemukan data.
                                </p>
                            </div>
                        </td>
                    </tr>
                    @endforelse
                  </table>
                </div>
                <div class="float-right">
                  {{ $materials->links() }}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

<form action="" method="POST" id="deleteForm">
    @csrf
    @method('DELETE')
    <input type="submit" style="display: none;">
</form>
@endsection

@section('js')

<script type="text/javascript">
    $('.btn-delete').on('click', function(){
        var href = $(this).attr('href');
        var title = $(this).data('title');
        swal({
          title: "Anda yakin akan menghapus Supplier bernama "+ title +" ?",
          text: "Setelah dihapus data tidak dapat dikembalikan !",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $('#deleteForm').attr('action', href);
            $('#deleteForm').submit();
          }
        });
    });
    </script>
@endsection
