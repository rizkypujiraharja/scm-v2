<link rel="stylesheet" href="{{ asset('/stisla-2.2.0/dist/assets/modules/bootstrap/css/bootstrap.min.css') }}">
<table class="table table-bordered">
    <thead>
        <tr>
            <th>No</th>
            <th>Kode Bahan Baku</th>
            <th>Nama</th>
            <th>Stock</th>
        </tr>
    </thead>
    <tbody>
    @foreach($reports as $key=>$item)
        <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $item->code }}</td>
            <td>{{ $item->name }}</td>
            <td>{{ $item->stock .' '. $item->unit_string }}</td>
        </tr>
    @endforeach
    </tbody>
</table>