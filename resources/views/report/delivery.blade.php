<link rel="stylesheet" href="{{ asset('/stisla-2.2.0/dist/assets/modules/bootstrap/css/bootstrap.min.css') }}">
<table class="table table-bordered">
    <thead>
        <tr>
            <th>No</th>
            <th>Kode Pengiriman</th>
            <th>Mobil</th>
            <th>Tanggal Pengiriman</th>
        </tr>
    </thead>
    <tbody>
    @foreach($reports as $key=>$item)
        <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $item->code }}</td>
            <td>{{ $item->car->merk}} {{ $item->car->no_pol }}</td>
            <td>{{ $item->tanggal_pengiriman }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
