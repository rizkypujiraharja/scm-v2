<!DOCTYPE html>
<html>
<head>
  <title>Report Order</title>
  <style type="text/css">
    .table {
        width: 100%;
        margin-bottom: 1rem;
        background-color: transparent;
        border-collapse: collapse;
    }

    .table-bordered th, .table-bordered td{
      border: 1px solid #dee2e6;
    }

    .table th, .table td{
      padding: 3px;
    }

    .align-center th, .align-center td{
      text-align: center;;
    }

    .align-left th, .align-left td{
      text-align: left;;
    }

    h3, h4{
      color: #a23234;
    }

  </style>
</head>
<body>
  <img src="{{ public_path('logo_small.png') }}" height="50px">
  <h3>Kode Pengiriman {{ $delivery->code }}</h3>
  <table class="table align-left">
    <tr>
        <th width="30%">Mobil</th>
        <td width="70%">{{ $delivery->car->merk}} {{ $delivery->car->no_pol }}</td>
    </tr>
    <tr>
        <th>Tanggal Pengiriman</th>
        <td>{{ $delivery->tanggal_pengiriman }}</td>
    </tr>
  </table>
  <h4>Data Pengiriman</h4>
  <table class="table table-bordered align-center">
    <tr>
      <th>No</th>
      <th>Nama Pelanggan</th>
      <th>Alamat Pelanggan</th>
      <th>Total Pembelian</th>
      <th>Tanda Terima</th>
    </tr>
    @foreach($delivery->details as $key=>$item)
    <tr>
        <td>{{ $key+1 }}</td>
        <td>{{ $item->order->customer_name }}</td>
        <td>{{ $item->order->customer_address }}</td>
        <td>{{ $item->order->total_harga }}</td>
        <td></td>
    </tr>
    @endforeach
  </table>

</body>
</html>
