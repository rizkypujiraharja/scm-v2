<!DOCTYPE html>
<html>
<head>
  <title>Report Production</title>
  <style type="text/css">
    .table {
        width: 100%;
        margin-bottom: 1rem;
        background-color: transparent;
        border-collapse: collapse;
    }

    .table-bordered th, .table-bordered td{
      border: 1px solid #dee2e6;
    }

    .table th, .table td{
      padding: 3px;
    }

    .align-center th, .align-center td{
      text-align: center;;
    }

    .align-left th, .align-left td{
      text-align: left;;
    }

    h3, h4{
      color: #a23234;
    }

  </style>
</head>
<body>
   <img src="{{ public_path('logo_small.png') }}" height="50px">
  <h3>Kode Produksi {{ $production->code }}</h3>
  <table class="table align-left">
    <tr>
        <th width="30%">Jumlah Produksi</th>
        <td width="70%">{{ $production->total_size }} Liter</td>
    </tr>
    <tr>
        <th>Tipe</th>
        <td>{!! $production->label_type !!}</td>
    </tr>
    <tr>
        <th>Estimasi Waktu</th>
        <td>{{ $production->estimated_days }} Hari</td>
    </tr>
    <tr>
        <th>Tanggal Produksi</th>
        <td>{{ $production->tanggal_produksi }}</td>
    </tr>
    <tr>
        <th>Status</th>
        <td>{!! $production->label_status !!}</td>
    </tr>
  </table>
  <h4>Data Produk</h4>
  <table class="table table-bordered align-center">
      <thead>
          <tr>
              <th>No</th>
              <th>Nama Produk</th>
              <th>Ukuran Produk</th>
              <th>Qty</th>
              <th>Sub Total</th>
          </tr>
      </thead>
      <tbody>
      @php $no = 1 @endphp
      @foreach ($products as $product => $details)
          @foreach ($details as $key => $item)
          <tr >
              @if ($key == 0)
              <td rowspan="{{ count($details) }}">{{ $no }}</td>
              <td rowspan="{{ count($details) }}">{{ $item->product_name }}</td>
              @endif
              <td>{{ $item->product_size }} Liter</td>
              <td>{{ $item->quantity }}</td>
              <td>{{ $item->sub_total_size}} Liter</td>
          </tr>
          @endforeach
          @php $no++ @endphp
      @endforeach
      </tbody>
  </table>

</body>
</html>