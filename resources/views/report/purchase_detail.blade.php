<!DOCTYPE html>
<html>
<head>
  <title>Report Purchase</title>
  <style type="text/css">
    .table {
        width: 100%;
        margin-bottom: 1rem;
        background-color: transparent;
        border-collapse: collapse;
    }

    .table-bordered th, .table-bordered td{
      border: 1px solid #dee2e6;
    }

    .table th, .table td{
      padding: 3px;
    }

    .align-center th, .align-center td{
      text-align: center;;
    }

    .align-left th, .align-left td{
      text-align: left;;
    }

    h3, h4{
      color: #a23234;
    }

  </style>
</head>
<body>
   <img src="{{ public_path('logo_small.png') }}" height="50px">
  <h3>Kode Pengadaan {{ $purchase->code }}</h3>
  <table class="table align-left">
    <tr>
        <th width="30%">Pemasok</th>
        <td width="70%">{{ $purchase->supplier->name }}</td>
    </tr>
    <tr>
        <th>No Telepon</th>
        <td>{{ $purchase->supplier->phone }}</td>
    </tr>
    <tr>
        <th>Alamat</th>
        <td>{{  $purchase->supplier->address  }} Hari</td>
    </tr>
    <tr>
        <th>Tanggal Produksi</th>
        <td>{{ $purchase->tanggal_pengadaan }}</td>
    </tr>
    <tr>
        <th>Status</th>
        <td>{!! $purchase->label_status !!}</td>
    </tr>
  </table>
  <h4>Detail Pengadaan</h4>
  <table class="table table-bordered align-center">
      <thead>
          <tr>
              <th>No</th>
              <th>Nama Bahan Baku</th>
              <th>Jumlah</th>
              <th>Harga</th>
              <th>Sub Total</th>
          </tr>
      </thead>
      <tbody>
        @foreach ($purchase->details as $key=>$item)
        <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $item->materialPrice->material->name }}</td>
            <td>{{ $item->quantity }} {{ $item->materialPrice->material->unit_string }}</td>
            <td>{{ $item->harga}}</td>
            <td>{{ $item->sub_total_harga}}</td>
        </tr>
        @endforeach
      </tbody>
  </table>

</body>
</html>