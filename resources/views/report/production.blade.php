<link rel="stylesheet" href="{{ asset('/stisla-2.2.0/dist/assets/modules/bootstrap/css/bootstrap.min.css') }}">
<table class="table table-bordered">
    <thead>
        <tr>
            <th>No</th>
            <th>Kode Produksi</th>
            <th>Jumlah</th>
            <th>Status</th>
            <th>Tipe</th>
            <th>Tanggal Produksi</th>
        </tr>
    </thead>
    <tbody>
    @foreach($reports as $key=>$item)
        <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $item->code }}</td>
            <td>{{ $item->total_size }} Liter</td>
            <td>{!! $item->label_status !!}</td>
            <td>{!! $item->label_type !!}</td>
            <td>{{ $item->tanggal_produksi }}</td>
        </tr>
    @endforeach
    </tbody>
</table>