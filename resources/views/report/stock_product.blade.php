<link rel="stylesheet" href="{{ asset('/stisla-2.2.0/dist/assets/modules/bootstrap/css/bootstrap.min.css') }}">
<table class="table table-bordered">
    <thead>
        <tr>
            <th>No</th>
            <th>Kode Produk</th>
            <th>Nama Produk</th>
            <th>Tipe Produk</th>
            <th>Ukuran</th>
            <th>Harga</th>
            <th>Stok</th>
        </tr>
    </thead>
    <tbody>
    @php $no = 1 @endphp
    @foreach ($reports as $index => $product)
        @foreach ($product->variants as $key => $item)
        <tr >
            @if ($key == 0)
            <td rowspan="{{ count($product->variants) }}">{{ $no }}</td>
            <td rowspan="{{ count($product->variants) }}">{{ $product->code }}</td>
            <td rowspan="{{ count($product->variants) }}">{{ $product->name }}</td>
            <td rowspan="{{ count($product->variants) }}">{{ $product->type }}</td>
            @endif
            <td>{{ $item->size }} Liter</td>
            <td>{{ $item->rupiah}}</td>
            <td>{{ $item->stock}}</td>
        </tr>
        @endforeach
        @php $no++ @endphp
    @endforeach
    </tbody>
</table>