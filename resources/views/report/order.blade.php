<link rel="stylesheet" href="{{ asset('/stisla-2.2.0/dist/assets/modules/bootstrap/css/bootstrap.min.css') }}">
<table class="table table-bordered">
    <thead>
        <tr>
            <th>No</th>
            <th>Kode Pesanan</th>
            <th>Nama Pemesan</th>
            <th>Total Harga</th>
            <th>Status</th>
            <th>Tipe</th>
            <th>Tanggal Pesanan</th>
        </tr>
    </thead>
    <tbody>
    @foreach($reports as $key=>$item)
        <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $item->invoice_code }}</td>
            <td>{{ $item->customer_name }}</td>
            <td>{{ $item->total_harga }}</td>
            <td>{!! $item->label_status !!}</td>
            <td>{!! $item->label_type !!}</td>
            <td>{{ $item->tanggal_pesan }}</td>
        </tr>
    @endforeach
    </tbody>
</table>