<link rel="stylesheet" href="{{ asset('/stisla-2.2.0/dist/assets/modules/bootstrap/css/bootstrap.min.css') }}">
<table class="table table-bordered">
    <thead>
        <tr>
            <th>No</th>
            <th>Kode Pengadaan</th>
            <th>Supplier</th>
            <th>Status</th>
            <th>Tanggal Pengadaan</th>
        </tr>
    </thead>
    <tbody>
    @foreach($reports as $key=>$item)
        <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $item->code }}</td>
            <td>{{ $item->supplier->name }}</td>
            <td>{!! $item->label_status !!}</td>
            <td>{{ $item->tanggal_pengadaan }}</td>
        </tr>
    @endforeach
    </tbody>
</table>