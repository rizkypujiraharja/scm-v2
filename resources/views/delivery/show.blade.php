@extends('layouts.default')

@section('content')
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('deliveries.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Detail Pengiriman</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
          <div class="breadcrumb-item"><a href="{{ route('deliveries.index') }}">Pengiriman</a></div>
          <div class="breadcrumb-item">Detail Pengiriman</div>
        </div>
      </div>
      <div class="section-body">
        <div class="row mt-4">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                  <h4>Data Pengiriman : {{$delivery->code }}</h4>
                  <div class="card-header-action">
                    <a href="{{ route('deliveries.show.report', $delivery) }}" class="btn btn-success mr-1">
                        Cetak
                    </a>
                  </div>
                </div>
                <div class="card-body">
                  <div class="row mb-2">
                    <div class="col-md-3">
                      <b>Mobil</b><br>
                      {{ $delivery->car->merk}} {{ $delivery->car->no_pol }}
                    </div>
                    <div class="col-md-3">
                      <b>Tanggal</b><br>
                      {{ $delivery->tanggal_pengiriman}}
                    </div>
                  </div>
                </div>
            </div>

            <div class="card">
              <div class="card-header">
                <h4>Detail Pengiriman</h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-striped">
                    <tr>
                      <th>No</th>
                      <th>Nama Pelanggan</th>
                      <th>Alamat Pelanggan</th>
                      <th>Total Pembelian</th>
                    </tr>
                    @foreach($delivery->details as $key=>$item)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $item->order->customer_name }}</td>
                        <td>{{ $item->order->customer_address }}</td>
                        <td>{{ $item->order->total_harga }}</td>
                    </tr>
                    @endforeach
                  </table>
                </div>
              </div>
            </div>
        </div>
      </div>
    </section>
  </div>
@endsection

@section('js')

@endsection
