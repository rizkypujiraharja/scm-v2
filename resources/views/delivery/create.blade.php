@extends('layouts.default')

@section('css')
    <style>
        td .custom-control{
            margin-top: 0.5rem;
        }
    </style>
@endsection

@section('content')
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('purchases.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Tambah Pengadaan</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
          <div class="breadcrumb-item"><a href="#">Pengadaan</a></div>
          <div class="breadcrumb-item">Tambah Pengadaan</div>
        </div>
      </div>


      <div class="section-body">
        <div class="row mt-4">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4>Data Pengiriman Baru</h4>
              </div>
              <div class="card-body">
                <form class="form" action="{{ route('deliveries.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group row align-items-center">
                        <label for="site-title" class="form-control-label col-sm-3 text-md-right">Mobil</label>
                        <div class="col-sm-6 col-md-9 col-lg-6">
                            <select name="car" class="@error('car') is-invalid @enderror form-control select2" required>
                                <option value="" readonly>Pilih Mobil</option>
                                @foreach ($cars as $item)
                                <option value="{{ $item->id }}" {{ old('car') == $item->id ? 'selected' : '' }}>{{ $item->merk }} {{ $item->no_pol }}</option>
                                @endforeach
                            </select>
                            @error('car')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row align-items-center">
                        <label for="site-title" class="form-control-label col-sm-3 text-md-right"></label>
                        <div class="col-sm-6 col-md-9 col-lg-6">
                            <h2 class="section-title">
                                Pilih Data Pesanan
                            </h2>

                            @error('order_ids')
                            <div class="text-danger text-small">
                                Wajib memilih data pesanan
                            </div>
                            @enderror
                            <table class="table form-table">
                                @foreach ($orders as $item)
                                <tr>
                                    <td class="p-0 text-center" style="width: 50px">
                                        <div class="custom-checkbox custom-control">
                                            <input type="checkbox" class="custom-control-input" id="checkbox-{{$item->id}}" value="{{$item->id}}" name="order_ids[]">
                                            <label for="checkbox-{{$item->id}}" class="custom-control-label">&nbsp;</label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="pt-3">
                                            <span class="text-primary font-weight-bold">{{$item->invoice_code}}</span>
                                            <br><span class="font-weight-bold">{{$item->customer_name}}</span>
                                            <p>{{$item->customer_address}}</p>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>


                    <div class="form-group row align-items-center">
                        <label for="site-title" class="form-control-label col-sm-3 text-md-right"></label>
                        <div class="col-sm-6 col-md-9 col-lg-6">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection

@section('js')
{{-- <script src="{{ asset("/js/vue-dev.js") }}"></script>
<script>
    function submit() {
        $('#form-product').submit()
    }

    var app = new Vue({
        el: '#app',
        data: {
            productions: {!! json_encode($productions) !!},
            products: {!! json_encode($products) !!},
            summary_productions: [],
            purchases:[],
            select_all:true
        },
        watch: {
            selectedProduction(){
                if(this.productions.length == this.selectedProduction.length){
                    this.select_all = true
                }else{
                    this.select_all = false
                }

                this.generateDetailPurchases()
            }
        },
        methods: {
            toggleSelectAll(){
                this.productions.forEach(production => {
                    production.selected = this.select_all
                });
            },
            generateDetailPurchases(){
                let selectedProd = JSON.parse(JSON.stringify(this.selectedProduction))

                // this.summary_productions = []
                this.summary_productions = Object.values(Object.values(selectedProd.map((production) => {
                        return production.details
                    }).flat(1)
                    .reduce((result, production) => {
                        if (typeof result[production.product_variant_id] === 'undefined') {
                            result[production.product_variant_id] = production
                        }else{
                            result[production.product_variant_id]['quantity'] += production.quantity
                        }
                        return result
                    }, {})).map((production) => {
                        let total_size = parseFloat((production.product_size * production.quantity).toFixed(2))
                        return {
                            product_id: production.product_variant.product_id,
                            product_name: production.product_name,
                            size: production.product_size,
                            quantity: production.quantity,
                            total_size: total_size,
                        }
                    }).reduce((result, production) => {
                        if (typeof result[production.product_id] === 'undefined') {
                            result[production.product_id] = production
                        }else{
                            result[production.product_id]['product_id'] = production.product_id
                            result[production.product_id]['product_name'] = production.product_name
                            result[production.product_id]['size'] += production.size
                            result[production.product_id]['quantity'] += production.quantity
                            result[production.product_id]['total_size'] += production.total_size
                        }
                        return result
                    }, {})
                )

                this.products.forEach(product => {
                    let production = this.summary_productions.find(production => production.product_id == product.id)
                    product.boms.map(bom => {
                        if(production){
                            bom.production_need = parseFloat((production.total_size * bom.dose).toFixed(2))
                            bom.min_order = Math.ceil(bom.material.prices[0].min_order >= bom.production_need ? bom.material.prices[0].min_order : bom.production_need)
                        }else{
                            bom.production_need = 0
                            bom.min_order = Math.ceil(bom.material.prices[0].min_order > bom.production_need ? bom.material.prices[0].min_order : bom.production_need)
                        }
                    })
                });

                this.purchases = []
                this.purchases = Object.values(
                    this.products.map(product => {
                        return product.boms
                    }).flat(1).filter(bom => {
                        return bom.production_need > 0
                    }).reduce((result, bom) => {
                        if (typeof result[bom.material_id] === 'undefined') {
                            result[bom.material_id] = bom
                            result[bom.material_id]['production_need'] = parseFloat(bom.production_need.toFixed(2))
                            result[bom.material_id]['min_order'] = bom.min_order
                        }else{
                            result[bom.material_id]['material_id'] = bom.material_id
                            result[bom.material_id]['production_need'] += bom.production_need
                        }
                        return result
                    }, {})
                ).map(purchase => {
                    purchase.production_need = parseFloat(purchase.production_need.toFixed(2)) - parseFloat(purchase.material.stock)
                    if(purchase.production_need > purchase.min_order){
                        purchase.min_order = purchase.production_need
                    }
                    return purchase
                })
            },
            getMinQuantity(key){
                quantity = document.getElementById('quantity'+key)
                production_need = document.getElementById('production_need'+key)
                material = document.getElementById('material'+key)

                if(production_need.value > parseInt(material.value.split('_')[1])){
                    quantity.value = production_need.value
                }else{
                    quantity.value = parseInt(material.value.split('_')[1])
                }

                quantity.setAttribute("min", quantity.value);
            }
        },
        computed:{
            selectedProduction(){
                return this.productions.filter(production=>{
                    return production.selected
                })
            }
        },
        created(){
            this.generateDetailPurchases()
        }
    })
</script> --}}
@endsection
