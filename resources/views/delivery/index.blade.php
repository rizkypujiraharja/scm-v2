@extends('layouts.default')

@section('css')
<link rel="stylesheet" href="{{asset('/stisla-2.2.0/dist/assets/modules/bootstrap-daterangepicker/daterangepicker.css')}}">
@endsection

@section('content')
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Pengiriman</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
          <div class="breadcrumb-item"><a href="#">Pengiriman</a></div>
        </div>
      </div>
      <div class="section-body">
        <div class="row mt-4">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4>Daftar Pengiriman</h4>
              </div>
              <div class="card-body">
                <div class="float-left">
                    <a href="{{ route('deliveries.create') }}" class="btn btn-primary">
                        Tambah Pengiriman
                        @if($notifDelivery > 0)
                        <span class="badge badge-light">{{ $notifDelivery }}</span>
                        @endif
                    </a>
                    <button onclick="report()" class="btn btn-success">Laporan Pengiriman</button>
                </div>
                <div class="float-right">
                  <form action="" method="GET" >
                    <div class="input-group">
                      @if(request()->daterange)
                        @php
                        $daterange=explode(" - ", request()->daterange);
                        $date_from = \Carbon\Carbon::createFromFormat('d/m/Y', $daterange[0]);
                        $date_to = \Carbon\Carbon::createFromFormat('d/m/Y', $daterange[1]);

                        $from = $date_from->format('d/m/Y');
                        $to = $date_to->format('d/m/Y');
                        @endphp
                        <input type="text" class="form-control daterange-cus mr-2" name="daterange" id="daterange" value="{{ $from }} - {{ $to }}">
                      @else
                      <input type="text" class="form-control daterange-cus mr-2" name="daterange" id="daterange" value="{{ date('d/m/Y', strtotime('-1 months'))}} - {{date("d/m/Y")}}">
                      @endif

                      {{-- <input type="text" class="form-control" name="search" placeholder="Search"> --}}
                      <div class="input-group-append">
                        <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                      </div>
                    </div>
                  </form>
                </div>

                <div class="clearfix mb-3"></div>

                <div class="table-responsive">
                  <table class="table table-striped">
                    <tr>
                      <th>No</th>
                      <th>Kode Pengiriman</th>
                      <th>Mobil</th>
                      <th>Tanggal</th>
                      <th>#</th>
                    </tr>
                    @forelse ($deliveries as $item)
                    <tr>
                        <td>{{ (($deliveries->currentPage()-1) * $deliveries->perPage()) + $loop->iteration }}</td>
                        <td>{{ $item->code }}</td>
                        <td>{{ $item->car->merk }} {{ $item->car->no_pol }}</td>
                        <td>{{ $item->tanggal_Pengiriman }}</td>
                        <td>
                            <a href="{{ route('deliveries.show', $item) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Lihat Pengiriman"> <span class="fa fa-eye"></span></a>
                            @if($item->created_at->format('Y-m-d') == date('Y-m-d'))
                            <span data-title="{{ $item->code }}" href="{{ route('deliveries.destroy', $item) }}" class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip" data-placement="top" title="Hapus Pesanan"> <span class="fa fa-trash"></span></span>
                            @endif
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="7" align="center">
                            <div class="empty-state" data-height="400" style="height: 400px;">
                                <div class="empty-state-icon">
                                  <i class="fas fa-question"></i>
                                </div>
                                <h2>Data Tidak Ditemukan</h2>
                                <p class="lead">
                                  Maaf kami tidak dapat menemukan data.
                                </p>
                            </div>
                        </td>
                    </tr>
                    @endforelse
                  </table>
                </div>
                <div class="float-right">
                  {{ $deliveries->links() }}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

<form action="" method="POST" id="deleteForm">
    @csrf
    @method('DELETE')
    <input type="submit" style="display: none;">
</form>
@endsection

@section('js')
<script src="{{asset('/stisla-2.2.0/dist/assets/modules/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script type="text/javascript">
  $('.daterange-cus').daterangepicker({
    locale: {format: 'DD/MM/YYYY'},
    drops: 'down',
    opens: 'left'
  });

</script>
<script type="text/javascript">
    $('.btn-delete').on('click', function(){
        var href = $(this).attr('href');
        var title = $(this).data('title');
        swal({
          title: "Anda yakin akan menghapus Pengiriman "+ title +" ?",
          text: "Setelah dihapus data tidak dapat dikembalikan !",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $('#deleteForm').attr('action', href);
            $('#deleteForm').submit();
          }
        });
    });

    function report(){
        daterange = $('#daterange').val()
        url = `{{url('deliveries')}}/report?daterange=${daterange}`
        window.location = url
    }
</script>
@endsection
