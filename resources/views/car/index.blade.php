@extends('layouts.default')

@section('content')
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Mobil</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
          <div class="breadcrumb-item"><a href="#">Mobil</a></div>
        </div>
      </div>
      <div class="section-body">
        <div class="row mt-4">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4>Daftar Mobil</h4>
              </div>
              <div class="card-body">
                <div class="float-left">
                    <a href="{{ route('cars.create') }}" class="btn btn-primary">Tambah Mobil</a>
                </div>

                <div class="clearfix mb-3"></div>

                <div class="table-responsive">
                  <table class="table table-striped">
                    <tr>
                      <th>No</th>
                      <th>No Pol</th>
                      <th>Merek</th>
                      <th>#</th>
                    </tr>
                    @forelse ($cars as $item)
                    <tr>
                        <td>{{ (($cars->currentPage()-1) * $cars->perPage()) + $loop->iteration }}</td>
                        <td>{{ $item->no_pol }}</td>
                        <td>{{ $item->merk }}</td>
                        <td>
                            {{-- <a href="" class="btn btn-sm btn-info"> <span class="fa fa-eye"></span></a> --}}
                            <a href="{{ route('cars.edit', $item) }}" class="btn btn-sm btn-warning"> <span class="fa fa-edit"></span></a>
                            <span data-title="{{ $item->no_pol }}" href="{{ route('cars.destroy', $item) }}" class="btn btn-sm btn-danger btn-delete"> <span class="fa fa-trash"></span></span>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="7" align="center">
                            <div class="empty-state" data-height="400" style="height: 400px;">
                                <div class="empty-state-icon">
                                  <i class="fas fa-question"></i>
                                </div>
                                <h2>Data Tidak Ditemukan</h2>
                                <p class="lead">
                                  Maaf kami tidak dapat menemukan data.
                                </p>
                            </div>
                        </td>
                    </tr>
                    @endforelse
                  </table>
                </div>
                <div class="float-right">
                  {{ $cars->links() }}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

<form action="" method="POST" id="deleteForm">
    @csrf
    @method('DELETE')
    <input type="submit" style="display: none;">
</form>
@endsection

@section('js')

<script type="text/javascript">
    $('.btn-delete').on('click', function(){
        var href = $(this).attr('href');
        var title = $(this).data('title');
        swal({
          title: "Anda yakin akan menghapus kendaraan "+ title +" ?",
          text: "Setelah dihapus data tidak dapat dikembalikan !",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $('#deleteForm').attr('action', href);
            $('#deleteForm').submit();
          }
        });
    });
    </script>
@endsection
