@extends('layouts.default')

@section('content')
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('products.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Detail Produk</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
          <div class="breadcrumb-item"><a href="{{ route('products.index') }}">Produk</a></div>
          <div class="breadcrumb-item">Detail Produk</div>
        </div>
      </div>
      <div class="section-body">
        <div class="row mt-4">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                  <h4>Data Produk</h4>
                </div>
                <div class="card-body">
                  <div class="row mb-2">
                    <div class="col-md-4">
                        <b>Nama</b><br>
                        {{ $product->name }}
                    </div>
                    <div class="col-md-4">
                        <b>Kode</b><br>
                        {{ $product->code }}
                    </div>
                    <div class="col-md-4">
                        <b>Jenis</b><br>
                        {{ ucfirst($product->type) }}
                    </div>
                  </div>

                  <div class="row mb-2">
                      <div class="col-md-5">
                          <h2 class="section-title">Bahan Baku</h2>
                          <table class="table table-striped">
                            <tr>
                                <th>No.</th>
                                <th>Bahan Baku</th>
                                <th>Takaran</th>
                            </tr>
                            @foreach ($product->boms as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->material->name }}</td>
                                <td>{{ $item->dose }} {{ $item->material->unit_string }}</td>
                            </tr>
                            @endforeach
                        </table>
                      </div>
                      <div class="col-md-7">
                          <h2 class="section-title">Variasi Produk</h2>
                            <table class="table table-striped">
                                <tr>
                                    <th>No.</th>
                                    <th>Ukuran</th>
                                    <th>Harga</th>
                                    <th>Stock</th>
                                </tr>
                                @foreach ($product->variants as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->size }} Liter</td>
                                    <td>{{ $item->rupiah }}</td>
                                    <td>{{ $item->stock }}</td>
                                </tr>
                                @endforeach
                            </table>
                      </div>
                  </div>
                </div>
            </div>
        </div>
      </div>
    </section>
  </div>
@endsection

@section('js')

@endsection
