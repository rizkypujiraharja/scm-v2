@extends('layouts.default')

@section('content')
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('products.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Tambah Produk</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
          <div class="breadcrumb-item"><a href="#">Produk</a></div>
          <div class="breadcrumb-item">Tambah Produk</div>
        </div>
      </div>
      <form class="form" action="{{ route('products.store') }}" method="POST" id="form-product">
      <div class="section-body">
        <div class="row mt-4">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4>Data Produk Baru</h4>
                <div class="card-header-action">
                    <button type="submit" class="btn btn-primary float-right" onclick="submit()">Simpan</button>
                </div>
              </div>
              <div class="card-body">
                @csrf
                <div class="form-group row align-items-center">
                    <label for="site-title" class="form-control-label col-sm-3 text-md-right">Nama Produk</label>
                    <div class="col-sm-6 col-md-9 col-lg-6">
                        <input type="text" value="{{ old('name') }}" name="name" class="@error('name') is-invalid @enderror form-control">
                        @error('name')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="form-group row align-items-center">
                    <label for="site-title" class="form-control-label col-sm-3 text-md-right">Jenis Produk</label>
                    <div class="col-sm-6 col-md-9 col-lg-6">
                        <select name="type" class="@error('type') is-invalid @enderror form-control select2">
                            <option value="sabun" {{ old('type') == 'sabun' ? 'selected' : '' }}>Sabun</option>
                            <option value="parfum" {{ old('type') == 'parfum' ? 'selected' : '' }}>Parfum</option>
                        </select>
                        @error('type')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12 col-lg-6">
                        <h2 class="section-title">
                            Komposisi Produk / 1 Liter
                            <button type="button" class="btn btn-sm btn-outline-success ml-3" @click="addBom">Tambah Bahan Baku</button>
                        </h2>
                        <table class="table form-table">
                            <thead>
                                <tr>
                                    <td class="form-group"><label>Bahan Baku</label></td>
                                    <td class="form-group"><label>Takaran</label></td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="bom, index in boms" :key="index">
                                    <td>
                                        <select v-model="bom.material" class="form-control" required>
                                            <option v-if="bom.material" :value="bom.material">@{{ bom.material.name }}</option>
                                            <option v-for="material, key in notSelectedMaterials" :value="material">@{{ material.name }}</option>
                                        </select>
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <input type="number" v-model="bom.dose" step="0.01" min="0.01" class="form-control" :name="'boms['+index+'][dose]'" required>
                                            <div class="input-group-append" v-if="bom.material">
                                                <div class="input-group-text">
                                                @{{ bom.material.unit_string }}
                                                </div>
                                            </div>
                                            <input type="hidden" :name="'boms['+index+'][material_id]'" :value="bom.material.id" v-if="bom.material">
                                        </div>
                                    </td>
                                    <td><button type="button" class="btn btn-sm btn-danger" @click="removeBom(index)"><span class="fa fa-trash"></span></button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <h2 class="section-title">
                            Variasi Produk
                            <button type="button" class="btn btn-sm btn-outline-success ml-3" @click="addVariant">Tambah Variasi</button>
                        </h2>
                        <table class="table form-table">
                            <thead>
                                <tr>
                                    <td class="form-group"><label>Ukuran</label></td>
                                    <td class="form-group"><label>Harga</label></td>
                                    <td class="form-group"><label>Stock</label></td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="variant, index in variants" :key="index">
                                    <td>
                                        <div class="input-group">
                                            <input type="number" v-model="variant.size" step="0.01" min="0.01" class="form-control" :name="'variants['+index+'][size]'" required>
                                            <div class="input-group-append">
                                                <div class="input-group-text">
                                                Liter
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="number" v-model="variant.price" step="100" min="100" class="form-control" :name="'variants['+index+'][price]'" required>
                                    </td>
                                    <td>
                                        <input type="number" v-model="variant.stock" step="1" min="0" class="form-control" :name="'variants['+index+'][stock]'" required>
                                    </td>
                                    <td><button type="button" class="btn btn-sm btn-danger" @click="removeVariant(index)"><span class="fa fa-trash"></span></button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                {{-- <div class="form-group row align-items-center">
                    <label for="site-title" class="form-control-label col-sm-3 text-md-right"></label>
                    <div class="col-sm-6 col-md-9 col-lg-6">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div> --}}
              </div>
            </div>
          </div>
        </div>
      </div>
      </form>
    </section>
  </div>
@endsection

@section('js')
<script src="{{ asset("/js/vue-dev.js") }}"></script>
<script>
    function submit() {
        $('#form-product').submit()
    }
    var app = new Vue({
        el: '#app',
        data: {
            materials: {!! json_encode($materials) !!},
            boms: [{
                material: null,
                dose: 0
            }],
            variants: [{
                size: 0,
                price: 0,
                stock: 0
            }],
        },
        computed: {
            notSelectedMaterials: function(){
                let selected = true
                const selectedMaterial = this.boms.filter(bom => {
                    if(bom.material != null) return true;
                })
                return this.materials.filter(function(material, index){
                    let selected = true;
                    selectedMaterial.forEach(bom => {
                        if(bom.material.id == material.id){
                            selected = false
                        }
                    });
                    return selected
                })
            }
        },
        methods: {
            addBom(){
                this.boms.push({
                    material: null,
                    dose: 0
                })
            },
            removeBom(index){
                if(this.boms.length > 1){
                    this.boms.splice(index, 1);
                }
            },
            addVariant(){
                this.variants.push({
                    size: 0,
                    price: 0,
                    stock: 0
                })
            },
            removeVariant(index){
                if(this.variants.length > 1){
                    this.variants.splice(index, 1);
                }
            }
        }
    })
</script>
@endsection
