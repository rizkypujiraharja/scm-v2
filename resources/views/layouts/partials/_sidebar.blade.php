@php
$notifOrder = 0;
$notifProduction = 0;
$notifPurchase = 0;
$notifDelivery = 0;
@endphp

@switch(\Auth::user()->role)
    @case("administrator")
        @php
            $notifOrder = \App\Models\Order::where('status', 0)->count();
            $notifProduction = \App\Models\Production::where('status', 2)->count();
            $notifPurchase = \App\Models\Production::where('status', 0)->count();
            $notifDelivery = \App\Models\Order::where('status', 2)->count();
        @endphp
        @break
    @case("bag_admin")
        @php
            $notifDelivery = \App\Models\Order::where('status', 2)->count();
        @endphp

        @break
    @case("bag_gudang")
        @php
            $notifPurchase = \App\Models\Production::where('status', 0)->count()
        @endphp

        @break
    @case("bag_produksi")
        @php
            $notifOrder = \App\Models\Order::where('status', 0)->count();
            $notifProduction = \App\Models\Production::where('status', 2)->count();
        @endphp
        @break
    @default

@endswitch

<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ route('index') }}"><img src="{{ asset('logo.png') }}" class="logo-l"></a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ route('index') }}"><img src="{{ asset('icon.png') }}" class="logo-m"></a>
        </div>
        <ul class="sidebar-menu">
            <li class="{{ request()->is('/') ? 'active' : '' }}"><a class="nav-link" href="{{ route('home') }}"><i class="fas fa-th-large"></i> <span>Dashboard</span></a></li>
@php
    $user = \Auth::user();
@endphp
            {{-- <li class="menu-header">Data Master</li> --}}
            @if ($user->role == 'administrator' || $user->role == 'admin')
                <li class="{{ request()->is('users*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('users.index') }}"><i class="fas fa-users"></i> <span>Data Pegawai</span></a></li>
                <li class="{{ request()->is('cars*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('cars.index') }}"><i class="fas fa-truck"></i> <span>Data Mobil</span></a></li>
            {{-- @endif

            @if ($user->role == 'administrator' || $user->role == 'admin') --}}
                <li class="{{ request()->is('customers*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('customers.index') }}"><i class="fas fa-user"></i> <span>Data Pelanggan</span></a></li>
            @endif

            @if ($user->role == 'administrator'|| $user->role == 'admin' || $user->role == 'bag_gudang')
                <li class="{{ request()->is('suppliers*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('suppliers.index') }}"><i class="fas fa-address-card"></i> <span>Data Supplier</span></a></li>
            @endif

            @if ($user->role == 'administrator'|| $user->role == 'bag_gudang')
                <li class="{{ request()->is('materials*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('materials.index') }}"><i class="fas fa-fill-drip"></i> <span>Data Bahan Baku</span></a></li>
            @endif

            @if ($user->role == 'administrator' || $user->role == 'bag_produksi')
                <li class="{{ request()->is('products*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('products.index') }}"><i class="fas fa-spray-can"></i> <span>Data Produk</span></a></li>
            @endif

            @if ($user->role == 'administrator' || $user->role == 'bag_produksi' || $user->role == 'admin')
                <li class="{{ request()->is('orders*') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('orders.index') }}">
                        <i class="fas fa-shopping-cart"></i>
                        <span>Pesanan</span>
                        @if($notifOrder > 0)
                        <span class="badge badge-warning" style="width: 30px;">{{ $notifOrder }}</span>
                        @endif
                    </a>
                </li>
            @endif

            @if ($user->role == 'administrator' || $user->role == 'bag_produksi')
                <li class="{{ request()->is('productions*') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('productions.index') }}">
                        <i class="fas fa-flask"></i>
                        <span>Produksi</span>
                        @if($notifProduction > 0)
                        <span class="badge badge-warning" style="width: 30px;">{{ $notifProduction }}</span>
                        @endif
                    </a>
                </li>
            @endif

            @if ($user->role == 'administrator' || $user->role == 'bag_gudang')
                <li class="{{ request()->is('purchases*') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('purchases.index') }}">
                        <i class="fas fa-dolly-flatbed"></i>
                        <span>Pengadaaan Bahan Baku</span>
                        @if($notifPurchase > 0)
                        <span class="badge badge-warning" style="width: 30px;">{{ $notifPurchase }}</span>
                        @endif
                    </a>
                </li>
            @endif

            @if ($user->role == 'administrator' || $user->role == 'admin')
                <li class="{{ request()->is('deliveries*') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('deliveries.index') }}">
                        <i class="fas fa-shipping-fast"></i>
                        <span>Pengiriman</span>
                        @if($notifDelivery > 0)
                        <span class="badge badge-warning" style="width: 30px;">{{ $notifDelivery }}</span>
                        @endif
                    </a>
                </li>
            @endif


            {{-- <li class="nav-item dropdown">
                <a href="#" class="nav-link has-d ropdown" data-toggle="dropdown"><i class="fas fa-columns"></i> <span>Data Master</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="layout-default.html"><i class="fas fa-users"></i> Data Pegawai</a></li>
                    <li><a class="nav-link" href="layout-transparent.html"><i class="fas fa-bong"></i> Data Produk</a></li>
                    <li><a class="nav-link" href="layout-top-navigation.html"><i class="fas fa-address-card"></i> Data Supplier</a></li>

                </ul>
            </li> --}}
        </ul>
    </aside>
</div>
