<footer class="main-footer">
    <div class="footer-left">
        Supply Chain Management
    </div>
    <div class="footer-right">
        Copyright &copy; 2020 <div class="bullet"></div> PT. Barokah Kreasi Solusindo
    </div>
</footer>
