@extends('layouts.default')

@section('content')
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('productions.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Detail Produksi</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
          <div class="breadcrumb-item"><a href="{{ route('productions.index') }}">Produksi</a></div>
          <div class="breadcrumb-item">Detail Produksi</div>
        </div>
      </div>
      <div class="section-body">
        <div class="row mt-4">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                  <h4>Kode Produksi {{$production->code }}</h4>
                  <div class="card-header-action">
                    <a href="{{ route('productions.show.report', $production) }}" class="btn btn-success mr-1">
                        Cetak
                    </a>

                    @if(\Auth::user()->role == 'administrator' || \Auth::user()->role == 'bag_produksi')
                    @switch($production->status)
                        @case(2)
                            <a href="{{ route('productions.toggle_status', $production) }}?status=3" class="btn btn-primary">
                                Produksi
                            </a>
                            @break
                        @case(3)
                            <a href="{{ route('productions.toggle_status', $production) }}?status=4" class="btn btn-primary">
                                Selesai
                            </a>
                            @break
                        @default

                    @endswitch
                    @endif
                  </div>
                </div>
                <div class="card-body">
                  <div class="row mb-2">
                    <div class="col-md-3">
                      <b>Jumlah Produksi</b><br>
                      {{ $production->total_size }} Liter<br>
                    </div>
                    <div class="col-md-3">
                      <b>Tipe</b><br>
                      {!! $production->label_type !!}<br><br>
                    </div>
                    <div class="col-md-3">
                      <b>Estimasi Waktu Produksi</b><br>
                      {{ $production->estimated_days }} Hari<br>
                    </div>
                    <div class="col-md-3">
                      <b>Status Produksi</b><br>
                      {!! $production->label_status !!}
                    </div>
                  </div>
                </div>
            </div>

            <div class="card">
              <div class="card-header">
                <h4>Detail Produksi</h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered">
                    <tr>
                      <th>No</th>
                      <th>Nama Produk</th>
                      <th>Ukuran Produk</th>
                      <th>Qty</th>
                      <th>Sub Total</th>
                    </tr>
                    @php $no = 1 @endphp
                    @forelse ($products as $product => $details)
                        @foreach ($details as $key => $item)
                        <tr >
                            @if ($key == 0)
                            <td rowspan="{{ count($details) }}">{{ $no }}</td>
                            <td rowspan="{{ count($details) }}">{{ $item->product_name }}</td>
                            @endif
                            <td>{{ $item->product_size }} Liter</td>
                            <td>{{ $item->quantity }}</td>
                            <td>{{ $item->sub_total_size }} Liter</td>
                        </tr>
                        @endforeach
                        @php $no++ @endphp
                    @empty
                    <tr>
                        <td colspan="4" align="center">
                            <div class="empty-state" data-height="400" style="height: 400px;">
                                <div class="empty-state-icon">
                                  <i class="fas fa-question"></i>
                                </div>
                                <h2>Data Tidak Ditemukan</h2>
                                <p class="lead">
                                  Maaf kami tidak dapat menemukan data.
                                </p>
                            </div>
                        </td>
                    </tr>
                    @endforelse
                  </table>
                </div>
              </div>
            </div>
        </div>
      </div>
    </section>
  </div>
@endsection

@section('js')

@endsection
