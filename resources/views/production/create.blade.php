@extends('layouts.default')

@section('content')
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('productions.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Tambah Produksi</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
          <div class="breadcrumb-item"><a href="#">Produksi</a></div>
          <div class="breadcrumb-item">Tambah Produksi</div>
        </div>
      </div>
      <div class="section-body">
        <div class="row mt-4">
          <div class="col-sm-5">
            <div class="card">
              <div class="card-header">
                <h4>Pilih Produk</h4>
                <div class="card-header-action">
                    <input type="text" v-model="search_product" class="form-control" placeholder="Cari Produk">
                </div>
              </div>
              <div class="card-body" style="height: 70vh; overflow-y: auto;overflow-x: hidden;">
                <div class="row mb-2" v-for="product, index in filterProduct" :key="index">
                    <div class="col-10">
                        @{{ product.name }}
                    </div>
                    <div class="col-2">
                        <button class="btn btn-primary btn-sm float-right" @click="addProductToCart(index)"><span class="fa fa-plus"></span></button>
                    </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-7">

            <form class="form" action="{{ route('productions.store') }}" method="POST" id="form-product">
                <div class="card">
                <div class="card-header">
                    <h4>Data Produksi</h4>
                    <div class="card-header-action">
                        <button type="submit" class="btn btn-primary float-right">Simpan</button>
                    </div>
                </div>
                <div class="card-body" style="height: 70vh; overflow-y: auto;overflow-x: hidden;">
                    @csrf
                    @error('production')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                    <div class="row" v-for="cart, index in carts" :key="index">
                        <div class="col-md-12">
                            <h2 class="section-title">
                                @{{ cart.product_name }}
                                <button class="btn btn-primary btn-sm float-right" type="button" @click="removeProductInCart(index)" style="border-radius:0.2rem !important"><span class="fa fa-trash"></span></button>
                            </h2>
                            <table class="table form-table">
                                <thead>
                                    <tr>
                                        <td class="form-group"><label>Ukuran</label></td>
                                        <td class="form-group"><label>Jumlah</label></td>
                                        <td class="form-group"><label>Total</label></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="item, key in cart.variants" :key="key">
                                        <input type="hidden" v-model="item.id" :name="'production['+item.id+'][product_variant_id]'">
                                        <input type="hidden" v-model="cart.product_name" :name="'production['+item.id+'][product_name]'">
                                        <td>
                                            <input type="hidden" v-model="item.size" :name="'production['+item.id+'][product_size]'">
                                            @{{ item.size }} Liter
                                        </td>
                                        <td>
                                            <input type="number" @change="getSubTotal(index, key)" v-model="item.qty" :name="'production['+item.id+'][quantity]'" min="0" class="form-control">
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <input type="number" v-model="item.sub_total_size" :name="'production['+item.id+'][sub_total_size]'" min="0" class="form-control" readonly>
                                                <div class="input-group-append">
                                                    <div class="input-group-text">
                                                    Liter
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection

@section('js')
<script src="{{ asset("/js/vue-dev.js") }}"></script>
<script>

    var app = new Vue({
        el: '#app',
        data: {
            search_product:'',
            products: {!! json_encode($products) !!},
            carts:[],
            temp_product:[],
        },
        methods: {
            getSubTotal(index, key_variant)
            {
                cart = this.carts[index]
                variant = cart.variants[key_variant]
                cart.variants[key_variant].sub_total_size = variant.qty*variant.size
                Vue.set(this.carts, index, cart)
            },
            addProductToCart(index){
                product = JSON.parse(JSON.stringify(this.products[index]))
                this.temp_product.push(product)
                variants = product.variants.map(function(item){
                    item.qty = 0
                    item.subtotal = 0
                    return item
                });

                this.carts.push({
                    product_id: product.id,
                    product_name: product.name,
                    variants:variants,
                })

                this.products.splice(index, 1);
            },
            removeProductInCart(index){
                // if(this.carts.length > 1){
                    product = this.temp_product[index]
                    this.products.push(product)

                    this.carts.splice(index, 1);
                    this.temp_product.splice(index, 1);
                // }
            },
        },
        computed:{
            filterProduct(){
                return this.products.filter(product=>{
                    return product.name.toLowerCase().includes(this.search_product.toLowerCase())
                })
            },
        },
    })
</script>
@endsection
