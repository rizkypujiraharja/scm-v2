@extends('layouts.default')
@section('css')
<style type="text/css">
    .card-recent {
      height: 250px;
    }
</style>
@endsection
@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Sistem Informasi Supply Chain Manajemen</h1>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div style="margin-top: 20vh">
                            <center>
                                <img src="{{ asset('logo.png') }}" style="width:300px;margin-bottom:35px">
                                <h2>
                                    PT. Barokah Kreasi Solusindo
                                </h2>
                                <hr>
                                <p style="font-size: 14pt">
                                    PT. Barokah Kreasi Solusindo berdiri sejak tahun 2015 bergerak sebagai produsen
                                    alat alat laundry dan produksi chemical laundry.
                                    Perusahaan ini memproduksi chemical laundry dari bahan baku dasar nantinya akan diolah menjadi
                                    berbagai macam chemical sabun dan parfum laundry untuk memenuhi hampir semua kebutuhan laundry
                                    yang ada di Indonesia.
                                </p>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
