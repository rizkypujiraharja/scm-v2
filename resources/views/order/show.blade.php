@extends('layouts.default')

@section('content')
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('orders.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Detail Pesanan</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
          <div class="breadcrumb-item"><a href="{{ route('orders.index') }}">Pesanan</a></div>
          <div class="breadcrumb-item">Detail Pesanan</div>
        </div>
      </div>
      <div class="section-body">
        <div class="row mt-4">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                  <h4>Invoice : {{$order->invoice_code }}</h4>
                  <div class="card-header-action">
                    <a href="{{route('orders.show.report', $order)}}" class="btn btn-success mr-1">
                        Cetak
                    </a>
                    @if(\Auth::user()->role == 'administrator' || \Auth::user()->role == 'bag_produksi')
                        @switch($order->status)
                            @case(0)
                                <a href="{{ route('orders.toggle_status', $order) }}?status=1" class="btn btn-primary">
                                    Produksi
                                </a>
                                @break
                        @endswitch
                    @endif
                  </div>
                </div>
                <div class="card-body">
                  <div class="row mb-2">
                    <div class="col-md-6">
                      <b>Alamat Pengiriman</b><br>
                      {{ $order->customer_name }}<br>
                      {{ $order->customer_phone }}<br>
                      {{ $order->customer_address }}
                    </div>
                    <div class="col-md-3">
                      <b>Estimasi Waktu</b><br>
                      {{ $order->estimated_days }} Hari<br><br>
                      <b>Total Harga</b><br>
                      {{ $order->total_harga }}
                    </div>
                    <div class="col-md-3">
                      <b>Status Pesanan</b><br>
                      {!! $order->label_status !!}<br><br>
                      <b>Tipe</b><br>
                      {!! $order->label_type !!}
                    </div>
                  </div>
                </div>
            </div>

            <div class="card">
              <div class="card-header">
                <h4>Data Produk</h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered">
                    <tr>
                      <th>No</th>
                      <th>Nama Produk</th>
                      <th>Ukuran Produk</th>
                    @if(\Auth::user()->role == 'administrator' || \Auth::user()->role == 'bag_produksi')
                      <th>Dari Stock</th>
                    @endif
                      <th>Qty</th>
                      <th>Harga</th>
                      <th>Sub Total</th>
                    </tr>
                    @php $no = 1 @endphp
                    @forelse ($products as $product => $details)
                        @foreach ($details as $key => $item)
                        <tr >
                            @if ($key == 0)
                            <td rowspan="{{ count($details) }}">{{ $no }}</td>
                            <td rowspan="{{ count($details) }}">{{ $item->product_name }}</td>
                            @endif
                            <td>{{ $item->product_size }} Liter</td>
                            @if(\Auth::user()->role == 'administrator' || \Auth::user()->role == 'bag_produksi')
                            <td>{{ $item->from_stock}}</td>
                            @endif
                            <td>{{ $item->quantity }}</td>
                            <td>{{ $item->harga}}</td>
                            <td>{{ $item->sub_total_harga}}</td>
                        </tr>
                        @endforeach
                        @php $no++ @endphp
                    @empty
                    <tr>
                        <td colspan="4" align="center">
                            <div class="empty-state" data-height="400" style="height: 400px;">
                                <div class="empty-state-icon">
                                  <i class="fas fa-question"></i>
                                </div>
                                <h2>Data Tidak Ditemukan</h2>
                                <p class="lead">
                                  Maaf kami tidak dapat menemukan data.
                                </p>
                            </div>
                        </td>
                    </tr>
                    @endforelse
                  </table>
                </div>
              </div>
            </div>
        </div>
      </div>
    </section>
  </div>
@endsection

@section('js')

@endsection
