@extends('layouts.default')
@section('css')
    <link rel="stylesheet" href="{{ asset("/css/vue-select.css") }}">

@endsection
@section('content')
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('orders.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Tambah Pesanan</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
          <div class="breadcrumb-item"><a href="#">Pesanan</a></div>
          <div class="breadcrumb-item">Tambah Pesanan</div>
        </div>
      </div>
      <div class="section-body">
        <div class="row mt-4">
          <div class="col-sm-5">
            <div class="card">
              <div class="card-header">
                <h4>Pilih Produk</h4>
                <div class="card-header-action">
                    <input type="text" v-model="search_product" class="form-control" placeholder="Cari Produk">
                </div>
              </div>
              <div class="card-body" style="height: 70vh; overflow-y: auto;overflow-x: hidden;">
                <div class="row mb-2" v-for="product, index in filterProduct" :key="index">
                    <div class="col-10">
                        @{{ product.name }}
                    </div>
                    <div class="col-2">
                        <button class="btn btn-primary btn-sm float-right" @click="addProductToCart(index)"><span class="fa fa-plus"></span></button>
                    </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-7">
            <form class="form" action="{{ route('orders.store') }}" method="POST" id="form-product">
            @csrf
            <div class="card">
              <div class="card-header">
                <h4>Data Pesanan</h4>
                <div class="card-header-action">
                    <span>(Antrian Produksi : @{{queue_production}} Liter) Estimasi Pengerjaan : <span>@{{ estimated_days }}</span> Hari</span>
                    <button type="submit" class="ml-3 btn btn-primary float-right">Simpan</button>
                </div>
              </div>
              <div class="card-body" style="height: 70vh; overflow-y: auto;overflow-x: hidden;">
                <button class="btn mr-1" :class="is_new_customer ? 'btn-primary' : 'btn-default'" @click="toggleCustomer(true)" type="button">Pelanggan Baru</button>
                <button class="btn mr-1" :class="!is_new_customer ? 'btn-primary' : 'btn-default'" @click="toggleCustomer(false)" type="button">Pelanggan Lama</button>
                <input type="hidden" name="is_new_customer" v-model="is_new_customer" value="{{old('is_new_customer')}}">
                <div class="mt-3" v-if="is_new_customer">
                    <div class="form-group row align-items-center">
                        <label for="site-title" class="form-control-label col-sm-3 text-md-right">Nama Pelanggan</label>
                        <div class="col-sm-6 col-md-9 col-lg-8">
                            <input type="text" name="customer_name" v-model="new_customer.name" class="@error('customer_name') is-invalid @enderror form-control" placeholder="Nama Lengkap Customer" :required="is_new_customer">
                            @error('customer_name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <label for="site-title" class="form-control-label col-sm-3 text-md-right">Telpon Pelanggan</label>
                        <div class="col-sm-6 col-md-9 col-lg-8">
                            <input type="text" name="customer_phone" v-model="new_customer.phone" class="@error('customer_phone') is-invalid @enderror form-control" placeholder="Telpon Customer":required="is_new_customer">
                            @error('customer_phone')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <label for="site-title" class="form-control-label col-sm-3 text-md-right">Email Pelanggan</label>
                        <div class="col-sm-6 col-md-9 col-lg-8">
                            <input type="email" name="customer_email" v-model="new_customer.email" class="@error('customer_email') is-invalid @enderror form-control" placeholder="Email Customer" :required="is_new_customer">
                            @error('customer_email')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <label for="site-title" class="form-control-label col-sm-3 text-md-right">Alamat Pelanggan</label>
                        <div class="col-sm-6 col-md-9 col-lg-8">
                            <input type="text" name="customer_address" v-model="new_customer.address" class="@error('customer_address') is-invalid @enderror form-control" placeholder="Alamat Customer" :required="is_new_customer">
                            @error('customer_address')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <label for="site-title" class="form-control-label col-sm-3 text-md-right">Tipe</label>
                        <div class="col-sm-6 col-md-9 col-lg-8">
                            <select name="type" v-model="new_type" class="@error('type') is-invalid @enderror form-control select2">
                                <option value="offline">Offline</option>
                                <option value="marketplace">Marketplace</option>
                            </select>
                            @error('type')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="mt-3" v-else="">
                    <div class="form-group col-sm-12">
                        <label class="form-control-label">Cari Pelanggan</label>
                        <v-select label="place_name" :filterable="false" @input="setCustomer" @search="fetchCustomer" :options="list_customer">
                            <template slot="no-options">
                                Cari Pelanggan . . .
                            </template>
                            <template slot="option" slot-scope="option">
                                <div class="d-center">
                                    @{{ option.name }}
                                </div>
                            </template>
                            <template slot="selected-option" slot-scope="option">
                                <div class="selected d-center">
                                    @{{ option.name }}
                                </div>
                            </template>
                        </v-select>
                    </div>
                    {{-- <div class="input-group mb-3">
                        <input type="text" class="form-control" name="search" placeholder="Cari Pelanggan">
                        <div class="input-group-append">
                            <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                        </div>
                    </div> --}}
                    <div v-show="has_customer">
                        <input type="hidden" name="customer_id" v-model="customer.id">
                        <div class="form-group row align-items-center">
                            <label for="site-title" class="form-control-label col-sm-3 text-md-right">Nama Pelanggan</label>
                            <div class="col-sm-6 col-md-9 col-lg-8">
                                <input type="text" v-model="customer.name" name="customer_name" class="@error('customer_name') is-invalid @enderror form-control" placeholder="Nama Lengkap Customer" :required="!is_new_customer">
                                @error('customer_name')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row align-items-center">
                            <label for="site-title" class="form-control-label col-sm-3 text-md-right">Telpon Pelanggan</label>
                            <div class="col-sm-6 col-md-9 col-lg-8">
                                <input type="text" v-model="customer.phone" name="customer_phone" class="@error('customer_phone') is-invalid @enderror form-control" placeholder="Telpon Customer" :required="!is_new_customer">
                                @error('customer_phone')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row align-items-center">
                            <label for="site-title" class="form-control-label col-sm-3 text-md-right">Email Pelanggan</label>
                            <div class="col-sm-6 col-md-9 col-lg-8">
                                <input type="email" v-model="customer.email" name="customer_email" class="@error('customer_email') is-invalid @enderror form-control" placeholder="Email Customer" :required="!is_new_customer">
                                @error('customer_email')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row align-items-center">
                            <label for="site-title" class="form-control-label col-sm-3 text-md-right">Alamat Pelanggan</label>
                            <div class="col-sm-6 col-md-9 col-lg-8">
                                <input type="text" v-model="customer.address" name="customer_address" class="@error('customer_address') is-invalid @enderror form-control" placeholder="Alamat Customer" :required="!is_new_customer">
                                @error('customer_address')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row align-items-center">
                            <label for="site-title" class="form-control-label col-sm-3 text-md-right">Tipe</label>
                            <div class="col-sm-6 col-md-9 col-lg-8">
                                <select name="type" class="@error('type') is-invalid @enderror form-control select2" v-model="type">
                                    <option value="offline">Offline</option>
                                    <option value="marketplace">Marketplace</option>
                                </select>
                                @error('type')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>


                    <hr>
                    <h6>List Pembelian Produk</h6>
                    @error('order')
                        <div class="text-small text-danger mt-2">
                            {{ $message }}
                        </div>
                    @enderror
                    <div class="row" v-for="cart, index in carts" :key="index">
                        <div class="col-md-12">
                            <h2 class="section-title">
                                @{{ cart.product_name }}
                                <button class="btn btn-primary btn-sm float-right" type="button" @click="removeProductInCart(index)"><span class="fa fa-trash"></span></button>
                            </h2>
                            <table class="table form-table">
                                <thead>
                                    <tr>
                                        <td class="form-group"><label>Ukuran</label></td>
                                        <td class="form-group"><label>Stock</label></td>
                                        <td class="form-group"><label>Harga</label></td>
                                        <td class="form-group"><label>Jumlah</label></td>
                                        <td class="form-group"><label>Total</label></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="item, key in cart.variants" :key="key">
                                        <input type="hidden" v-model="item.id" :name="'order['+item.id+'][product_variant_id]'">
                                        <input type="hidden" v-model="cart.product_name" :name="'order['+item.id+'][product_name]'">
                                        <td>
                                            <input type="hidden" :value="item.size" :name="'order['+item.id+'][product_size]'">
                                            @{{ item.size }} Liter
                                        </td>
                                        <td>
                                            <input type="hidden" v-model="item.stock"  :name="'order['+item.id+'][stock]'">
                                            @{{ item.stock }}
                                        </td>
                                        <td>
                                            <input type="hidden" v-model="item.price"  :name="'order['+item.id+'][price]'">
                                            @{{ item.price }}
                                        </td>
                                        <td>
                                            {{-- <input type="hidden" :name="'order['+index+'][qty]'" v-model="item.qty"> --}}
                                            <input @change="getSubTotal(index, key)"  type="number" v-model="item.qty" :name="'order['+item.id+'][quantity]'" min="0" class="form-control">
                                        </td>
                                        <td>
                                            <input type="hidden" v-model="item.sub_total_price" :name="'order['+item.id+'][sub_total_price]'">
                                            @{{ item.sub_total_price }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection

@section('js')
<script src="{{ asset("/js/vue-dev.js") }}"></script>
<script src="{{ asset("/js/vue-select.js") }}"></script>

<script>
    let is_new_customer = true;
    let customer_id, customer_name, customer_address, customer_email, customer_phone, type
    let has_old = false
    if('{{old('is_new_customer')}}'){
        has_old = true
        is_new_customer = ('{{old('is_new_customer')}}' === 'true')
        customer_id = '{{old('customer_id')}}'
        customer_name = '{{old('customer_name')}}'
        customer_address = '{{old('customer_address')}}'
        customer_phone = '{{old('customer_phone')}}'
        customer_email = '{{old('customer_email')}}'
        type = '{{old('type')}}'
    }
    function submit() {
        $('#form-product').submit()
    }

    var app = new Vue({
        el: '#app',
        components: {
            'v-select':VueSelect.VueSelect
        },
        watch: {
            carts: function (newQuestion, oldQuestion) {
                this.generateEstimatedDays()
            }
        },
        data: {
            queue_production:{{$totalQueueProductions}},
            estimated_days:0,
            is_new_customer:is_new_customer,
            has_customer:false,
            search_product:'',
            products: {!! json_encode($products) !!},
            customers: {!! json_encode($customers) !!},
            carts:[],
            temp_product:[],
            list_customer:[],
            customer:{},
            new_customer:{},
            type:'offline',
            new_type:'offline',
        },
        methods: {
            toggleCustomer(bool)
            {
                this.is_new_customer = bool
            },
            getSubTotal(index, key_variant)
            {
                cart = this.carts[index]
                variant = cart.variants[key_variant]
                cart.variants[key_variant].sub_total_price = variant.qty*variant.price
                Vue.set(this.carts, index, cart)
            },
            addProductToCart(index){
                product = JSON.parse(JSON.stringify(this.products[index]))
                this.temp_product.push(product)
                variants = product.variants.map(function(item){
                    item.qty = 0
                    item.sub_total_price = 0
                    return item
                });

                this.carts.push({
                    product_id: product.id,
                    product_name: product.name,
                    variants:variants,
                })

                this.products.splice(index, 1);
            },
            removeProductInCart(index){
                // if(this.carts.length > 1){
                    product = this.temp_product[index]
                    this.products.push(product)

                    this.carts.splice(index, 1);
                    this.temp_product.splice(index, 1);
                // }
            },
            fetchCustomer(search,loading)
            {
                this.list_customer = this.customers.filter(customer=>{
                    if(!search.length){
                        return true
                    }else{
                        return customer.name.toLowerCase().includes(search.toLowerCase())
                    }
                })
            },
            setCustomer(value)
            {
                this.has_customer=true
                this.customer=value
            },
            generateEstimatedDays(){
                let total_qty_order = 0
                let orders = []
                let carts = JSON.parse(JSON.stringify(this.carts))
                carts.forEach(cart => {
                    orders = orders.concat(cart.variants)
                });

                orders = orders.filter(function(order){
                    return order.qty > order.stock
                }).map(function(order){
                    return order.qty * order.size
                })

                if(orders.length){
                    total_qty_order = orders.reduce(function(total, num) {
                        return total + num;
                    });
                }

                if(total_qty_order){
                    let total_production = total_qty_order + this.queue_production;
                    this.estimated_days = 2 + Math.ceil(total_production / {{config('app.max_production')}});
                }else{
                    this.estimated_days = 0;
                }
            }
        },
        created(){
            if(has_old){
                if(!is_new_customer){
                    this.customer={
                        id:customer_id,
                        name:customer_name,
                        address:customer_address,
                        phone:customer_phone,
                        email:customer_email,
                    }
                    this.type = type
                    this.has_customer=true
                } else {
                    this.new_customer={
                        id:customer_id,
                        name:customer_name,
                        address:customer_address,
                        phone:customer_phone,
                        email:customer_email,
                    }
                    this.new_type = type
                }

            }
            this.fetchCustomer('', false)
            this.generateEstimatedDays()
        },
        computed:{
            filterProduct(){
                return this.products.filter(product=>{
                    return product.name.toLowerCase().includes(this.search_product.toLowerCase())
                })
            },
        },
    })
</script>
@endsection
