<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');


Route::group(['middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('index');
    Route::get('/dashboard', 'HomeController@dashboard')->name('home');

    Route::resource('users', 'UserController');
    Route::resource('suppliers', 'SupplierController');
    Route::get('materials/report', 'MaterialController@report')->name('materials.report');
    Route::resource('materials', 'MaterialController');
    Route::resource('cars', 'CarController');
    Route::group(['prefix' => 'suppliers'], function () {
        Route::get('/{supplier}/materials', 'MaterialPriceController@index')->name('suppliers.materials.index');
        Route::get('/{supplier}/materials/create', 'MaterialPriceController@create')->name('suppliers.materials.create');
        Route::post('/{supplier}/materials/store', 'MaterialPriceController@store')->name('suppliers.materials.store');
        Route::get('/{supplier}/materials/{material}/edit', 'MaterialPriceController@edit')->name('suppliers.materials.edit');
        Route::put('/{supplier}/materials{material}/update', 'MaterialPriceController@update')->name('suppliers.materials.update');
        Route::delete('/{supplier}/materials/{material}', 'MaterialPriceController@destroy')->name('suppliers.materials.delete');
    });

    Route::get('products/report', 'ProductController@report')->name('products.report');
    Route::resource('products', 'ProductController');

    Route::get('orders/report', 'OrderController@report')->name('orders.report');
    Route::resource('orders', 'OrderController');
    Route::get('/orders/{order}/report', 'OrderController@printDetail')->name('orders.show.report');
    Route::get('/orders/{order}/toggle-status', 'OrderController@toggleStatus')->name('orders.toggle_status');

    Route::get('productions/report', 'ProductionController@report')->name('productions.report');
    Route::resource('productions', 'ProductionController');
    Route::get('/productions/{production}/report', 'ProductionController@printDetail')->name('productions.show.report');
    Route::get('/productions/{production}/toggle-status', 'ProductionController@toggleStatus')->name('productions.toggle_status');

    Route::get('purchases/report', 'PurchaseController@report')->name('purchases.report');
    Route::resource('purchases', 'PurchaseController');
    Route::get('/purchases/{purchase}/report', 'PurchaseController@printDetail')->name('purchases.show.report');
    Route::get('/purchases/{purchase}/toggle-status', 'PurchaseController@toggleStatus')->name('purchases.toggle_status');

    Route::get('customers', 'CustomerController@index')->name('customers.index');
    Route::get('customers/report', 'CustomerController@report')->name('customers.report');
    Route::get('customers/{customer}', 'CustomerController@show')->name('customers.show');
    Route::get('customers/{customer}/edit', 'CustomerController@edit')->name('customers.edit');
    Route::get('customers/{customer}/update', 'CustomerController@update')->name('customers.update');

    Route::get('deliveries/report', 'DeliveryController@report')->name('deliveries.report');
    Route::resource('deliveries', 'DeliveryController');
    Route::get('deliveries/{delivery}/report', 'DeliveryController@printDetail')->name('deliveries.show.report');
});
