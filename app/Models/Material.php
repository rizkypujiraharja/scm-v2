<?php

namespace App\Models;

use Illuminate\Database\Eloquent\{Model, SoftDeletes};

class Material extends Model
{
    use SoftDeletes;
    protected $appends = ['unit_string'];

    public function getUnitStringAttribute()
    {
        return $this->unit == 'l' ? 'Liter' : 'Gram';
    }

    public function generateCode($name)
    {
        $name = strtoupper($name);
        $code = $name[0];
        $name = substr($name, 1);
        $name = preg_replace("/[^A-Z]+/", "", $name);
        $vowels = ["A", "E", "I", "O", "U", " "];
        $capital = str_replace($vowels, "", $name);
        $code .= $capital[0];

        $last = Self::where('code', 'LIKE', $code.'%')->latest()->first();

        if(is_null($last)){
            $code .= '001';
        }else{
            $lastNum = (int) substr($last->code, 1, 4);
            $code .= sprintf("%'03d", $lastNum+1);
        }

        return $code;
    }

    public function prices()
    {
        return $this->hasMany(MaterialPrice::class)->orderBy('price');
    }
}
