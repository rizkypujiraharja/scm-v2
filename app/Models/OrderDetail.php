<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
	protected $fillable = [
		'product_variant_id','product_name','product_size','quantity','price','sub_total_price','from_stock'
	];

    protected $guarderd = [];

    public function getHargaAttribute(){
        return "Rp. " . number_format($this->price, 0, ',', '.');
    }

    public function getSubTotalHargaAttribute(){
        return "Rp. " . number_format($this->sub_total_price, 0, ',', '.');
    }

    public function productVariant()
    {
        return $this->belongsTo(ProductVariant::class);
    }
}
