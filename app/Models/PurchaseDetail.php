<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseDetail extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function materialPrice()
    {
        return $this->belongsTo(MaterialPrice::class);
    }

    public function getHargaAttribute(){
        return "Rp. " . number_format($this->price, 0, ',', '.');
    }

    public function getSubTotalHargaAttribute(){
        return "Rp. " . number_format($this->sub_total_price, 0, ',', '.');
    }
}
