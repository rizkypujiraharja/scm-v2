<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductionDetail extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function productVariant()
    {
        return $this->belongsTo(ProductVariant::class);
    }
}
