<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseProduction extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function production()
    {
        return $this->belongsTo(Production::class);
    }
}
