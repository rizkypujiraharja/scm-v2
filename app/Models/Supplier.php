<?php

namespace App\Models;

use Illuminate\Database\Eloquent\{Model, SoftDeletes};

class Supplier extends Model
{
    use SoftDeletes;

    public function materials()
    {
        return $this->hasMany(MaterialPrice::class);
    }

    public function generateCode()
    {
        $last = Self::latest()->first();
        $code = 'S';
        if(is_null($last)){
            $code .= '001';
        }else{
            $lastNum = (int) substr($last->code, 1, 3);
            $code .= sprintf("%'03d", $lastNum+1);
        }

        return $code;
    }
}
