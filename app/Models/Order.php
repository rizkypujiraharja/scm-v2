<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['status'];
	protected $appends=['label_status','tanggal_pesan'];

    public function generateCode()
    {
        $monthyear = date('my');
        $currentCode = '/BKS/UTM/'.$monthyear;

        $last = Self::where('invoice_code', 'LIKE', '%'.$currentCode)->latest()->first();

        if(is_null($last)){
            $code = '00001'.$currentCode;
        }else{
            $lastNum = (int) substr($last->invoice_code, 1, 5);
            $code = sprintf("%'05d", $lastNum+1).$currentCode;
        }

        return $code;
    }

    public function getEstimatedDay($orders)
    {
        $production = Self::with(['details'])->where('status', '<', 2)->get();
        $totalQueueProductions = $production->map(function ($order){
            $order->total_size = $order->details->map(function($detail){
                $detail->total_size = $detail->product_size * $detail->quantity;
                return $detail;
            })->sum('total_size');
            return $order;
        })->sum('total_size');
        $sum = (int) $totalQueueProductions;
        foreach ($orders as $key => $value) {
            $order = (object) $value;
            $liter = $order->product_size * $order->quantity;
            $sum += $liter;
        }
        $estimated_day = 2 + ceil($sum/config('app.max_production'));
        return (int) $estimated_day;
    }

    public function details()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function delivery()
    {
        return $this->hasOne(DeliveryDetail::class);
    }

    public function getLabelStatusAttribute()
    {
    	switch ($this->status) {
    		case 0:
    			return '<span class="badge badge-warning">Menunggu Produksi</span>';
    		case 1:
    			return '<span class="badge badge-primary">Produksi</span>';
    		case 2:
    			return '<span class="badge badge-info">Siap kirim</span>';
    		case 3:
    			return '<span class="badge badge-success">Dikirim</span>';
    	}
    }

    public function getTanggalPesanAttribute()
    {
        return $this->created_at->isoFormat('dddd, D MMMM Y HH:mm');
    }

    public function getTotalHargaAttribute(){
        return "Rp. " . number_format($this->total_price, 0, ',', '.');
    }

    public function getLabelTypeAttribute()
    {
    	if($this->type == 'offline'){
            return '<span class="badge badge-secondary">Offline</span>';
        }else{
            return '<span class="badge badge-light">Online</span>';
        }
    }
}
