<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MaterialPrice extends Model
{
    protected $guarded = [];
    protected $appends = ['rupiah'];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function material()
    {
        return $this->belongsTo(Material::class);
    }

    public function getRupiahAttribute()
    {
        return "Rp. " . number_format($this->price, 0, ',', '.');
    }
}
