<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $appends = ['tanggal_pengadaan'];

    public function generateCode()
    {
        $monthyear = date('my');
        $currentCode = '/BKS/PRS/'.$monthyear;

        $last = Self::where('code', 'LIKE', '%'.$currentCode)->latest()->first();

        if(is_null($last)){
            $code = '00001'.$currentCode;
        }else{
            $lastNum = (int) substr($last->code, 1, 5);
            $code = sprintf("%'05d", $lastNum+1).$currentCode;
        }

        return $code;
    }

    public function details()
    {
        return $this->hasMany(PurchaseDetail::class);
    }

    public function productions()
    {
        return $this->hasMany(PurchaseProduction::class);
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function getLabelStatusAttribute()
    {
    	switch ($this->status) {
    		case 0:
    			return '<span class="badge badge-warning">Menunggu Penerimaan</span>';
    		case 1:
    			return '<span class="badge badge-success">Sudah Diterima</span>';
    	}
    }

    public function getTanggalPengadaanAttribute()
    {
        return $this->created_at->isoFormat('dddd, D MMMM Y HH:mm');
    }
}
