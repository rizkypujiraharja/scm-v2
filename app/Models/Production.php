<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Production extends Model
{
    protected $appends = ['label_status','tanggal_produksi','label_type'];

    public function details()
    {
        return $this->hasMany(ProductionDetail::class);
    }

    public function generateCode()
    {
        $monthyear = date('my');
        $currentCode = '/BKS/PRD/'.$monthyear;

        $last = Self::where('code', 'LIKE', '%'.$currentCode)->latest()->first();

        if(is_null($last)){
            $code = '00001'.$currentCode;
        }else{
            $lastNum = (int) substr($last->code, 1, 5);
            $code = sprintf("%'05d", $lastNum+1).$currentCode;
        }

        return $code;
    }

    public function getTanggalProduksiAttribute()
    {
        return $this->created_at->isoFormat('dddd, D MMMM Y HH:mm');
    }

    public function getLabelStatusAttribute()
    {
    	switch ($this->status) {
    		case 0:
    			return '<span class="badge badge-warning">Menunggu Pengadaan</span>';
            case 1:
                return '<span class="badge badge-info">Menunggu Penerimaan Pengadaan</span>';
    		case 2:
    			return '<span class="badge badge-dark">Siap Produksi</span>';
            case 3:
                return '<span class="badge badge-primary">Sedang Produksi</span>';
    		case 4:
    			return '<span class="badge badge-success">Selesai</span>';
    	}
    }

    public function getLabelTypeAttribute()
    {
    	if($this->type == 'stock'){
            return '<span class="badge badge-secondary">Stock</span>';
        }else{
            return '<span class="badge badge-light">Pesanan</span>';
        }
    }

    public function getEstimatedDaysAttribute()
    {
        return 1 + ceil($this->total_size/config('app.max_production'));
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
