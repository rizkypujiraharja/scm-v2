<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    public function generateCode()
    {
        $monthyear = date('my');
        $currentCode = '/BKS/DLV/'.$monthyear;

        $last = Self::where('code', 'LIKE', '%'.$currentCode)->latest()->first();

        if(is_null($last)){
            $code = '00001'.$currentCode;
        }else{
            $lastNum = (int) substr($last->code, 1, 5);
            $code = sprintf("%'05d", $lastNum+1).$currentCode;
        }

        return $code;
    }

    public function car()
    {
        return $this->belongsTo(Car::class);
    }

    public function details()
    {
        return $this->hasMany(DeliveryDetail::class);
    }

    public function getTanggalPengirimanAttribute()
    {
        return $this->created_at->isoFormat('dddd, D MMMM Y');
    }
}
