<?php

namespace App\Models;

use Illuminate\Database\Eloquent\{Model, SoftDeletes};

class ProductVariant extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    public function getRupiahAttribute()
    {
        return "Rp. " . number_format($this->price, 0, ',', '.');
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
