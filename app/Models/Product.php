<?php

namespace App\Models;

use Illuminate\Database\Eloquent\{Model, SoftDeletes};

class Product extends Model
{
    use SoftDeletes;

    public function generateCode($type)
    {
        if($type == 'parfum'){
            $code = 'P';
        }else{
            $code = 'C';
        }

        $last = Self::where('code', 'LIKE', $code.'%')->latest()->first();

        if(is_null($last)){
            $code .= '001';
        }else{
            $lastNum = (int) substr($last->code, 1, 3);
            $code .= sprintf("%'03d", $lastNum+1);
        }

        return $code;
    }

    public function boms()
    {
        return $this->hasMany(BomProduct::class);
    }

    public function variants()
    {
        return $this->hasMany(ProductVariant::class);
    }
}
