<?php

namespace App\Models;
use Illuminate\Database\Eloquent\{Model, SoftDeletes};

class Customer extends Model
{
    use SoftDeletes;

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function generateCode()
    {
        $last = Self::latest()->first();
        $code = 'C';
        if(is_null($last)){
            $code .= '001';
        }else{
            $lastNum = (int) substr($last->code, 1, 3);
            $code .= sprintf("%'03d", $lastNum+1);
        }

        return $code;
    }
    
}
