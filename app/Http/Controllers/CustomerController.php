<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Excel;
use App\Exports\CustomerReport;
use Carbon\Carbon;

class CustomerController extends Controller
{
    public function index(Request $request)
    {
        $customers = Customer::latest();

         if($request->has('search')){
            $query = '%'.$request->search.'%';
            $customers->where('name', 'LIKE', $query);
        }

        $customers = $customers->paginate(config('app.per_page'));

        return view('customer.index', compact('customers'));
    }

    public function show(Customer $customer)
    {
        return view('customer.show', compact('customer'));
    }

    public function edit(Customer $customer)
    {
        return view('customer.edit', compact('customer'));
    }

    public function update(Request $request, Customer $customer)
    {
        $this->validate($request, [
            'name' => 'required|string|min:3',
            'phone' => 'required|string',
            'email' => 'required|string',
            'address' => 'required|string',
        ], [], [
            'name' => 'Nama Customer',
            'phone' => 'No Telepon Customer',
            'email' => 'Email Customer',
            'address' => 'Alamat Customer',
        ]);

        $customer->name = $request->name;
        $customer->phone = $request->phone;
        $customer->email = $request->email;
        $customer->address = $request->address;
        $customer->save();

        return redirect()->route('customers.index')->with('alert-success', 'Berhasil mengubah data customer');
    }

    public function report(Request $request)
    {
        $date = Carbon::now()->isoFormat('D MMMM Y');

        $customers = Customer::get();

        $filename = 'Report Customer '.$date;
        return Excel::download(new CustomerReport($customers), $filename.'.csv');

        // return view('report.customer', compact('customers'));
    }

}
