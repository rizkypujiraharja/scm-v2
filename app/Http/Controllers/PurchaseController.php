<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Purchase, PurchaseDetail, PurchaseProduction};
use App\Models\{Production, Product, MaterialPrice};

use Carbon\Carbon;
use Excel;
use PDF;
use App\Exports\{PurchaseReport};

class PurchaseController extends Controller
{
    public function index(Request $request)
    {
        $purchases = Purchase::latest();

        if($request->has('search')){
            $query = '%'.$request->search.'%';
            $purchases->where('customer_name', 'LIKE', $query);
        }

        if($request->daterange){
            $daterange=explode(" - ", $request->daterange);
            $date_from = Carbon::createFromFormat('d/m/Y', $daterange[0]);
            $date_to = Carbon::createFromFormat('d/m/Y', $daterange[1]);

            $from = $date_from->format('Y-m-d');
            $to = $date_to->format('Y-m-d');
        } else {
            $from = date("Y-m-d", strtotime('-1 months'));
            $to = date("Y-m-d");
        }

        $purchases->whereDate('created_at','>=', $from)
                ->whereDate('created_at','<=', $to);

        $purchases = $purchases->paginate(config('app.per_page'));

        $needPurchase = Production::whereStatus(0)->count();
        return view('purchase.index', compact('purchases', 'needPurchase'));
    }

    public function create(Request $request)
    {
        $productions = Production::whereStatus(0)->with(['details' => function($query){
            $query->with('productVariant');
        }])->get()->map(function($production){
            $production->selected = true;
            return $production;
        });

        $productVariantIds = $productions->map(function($production){
            return $production->details;
        })->collapse()->pluck('product_variant_id')->unique();

        $products = Product::with('boms.material.prices.supplier')
                    ->whereHas('variants', function ($query) use ($productVariantIds){
                        $query->whereIn('id', $productVariantIds);
                    })->get();


        return view('purchase.create', compact('productions', 'products'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'production_ids' => 'required|array|min:1',
            'purchases' => 'required|array'
        ],[
            'production_ids.required' => 'Pilih data produksi minimal 1'
        ],[
            'production_ids' => 'produksi',
            'purchases' => 'pengadaan',
        ]);

        try {
            \DB::beginTransaction();
            $detailPurchases = collect($request->purchases)
                                ->map(function($detail){
                                    $id = explode('_', $detail['material_price_id'])[0];

                                    $material = MaterialPrice::find($id);
                                    $material->material->stock -= $detail['from_stock'];
                                    $material->material->save();

                                    $detail['material_price_id'] = $id;
                                    $detail['price'] = $material->price;
                                    $detail['supplier_id'] = $material->supplier_id;
                                    $detail['sub_total_price'] = $detail['quantity'] * $detail['price'];
                                    return $detail;
                                })->filter(function($detail){
                                    return $detail['production_need'] > 0;
                                })->groupBy('supplier_id');

            $productionIds = collect($request->production_ids)->map(function($id){
                return ['production_id' => (int)$id];
            })->toArray();

            foreach ($detailPurchases as $supplierId => $detail) {
                $detail = $detail->map(function($d){
                    unset($d['supplier_id']);
                    return $d;
                })->toArray();
                $purchase = new Purchase;
                $purchase->code = $purchase->generateCode();
                $purchase->status = 0;
                $purchase->supplier_id = $supplierId;
                $purchase->save();

                $purchase->productions()->createMany($productionIds);

                $purchase->details()->createMany($detail);

                Production::whereIn('id', $request->production_ids)->update(['status' => 1]);
            }
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            return redirect()->back()->withInput()->with('alert-error', 'Terjadi kesalahan server');
        }

        return redirect()->route('purchases.index')->with('alert-success', 'Berhasil Menambah Pengadaan');
    }

    public function show(Purchase $purchase)
    {
        $purchase->load('supplier', 'details.materialPrice.material');
        return view('purchase.show', compact('purchase'));
    }


    public function toggleStatus(Request $request, Purchase $purchase)
    {
        try {
            \DB::beginTransaction();
            $purchase->status = $request->status;
            $purchase->save();

            $purchase->load('details.materialPrice.material', 'productions.production.details.ProductVariant.product.boms');

            foreach ($purchase->details as $detail) {
                $detail->materialPrice->material->increment('stock', $detail->quantity - $detail->production_need);
            }

            $productionGroup = $purchase->productions->pluck('production_id')->toArray();
            $purchaseGroup = PurchaseProduction::whereIn('production_id', $productionGroup)->pluck('purchase_id')->toArray();

            $purchaseAcceptedGroup = Purchase::whereIn('id', $purchaseGroup)->where('status', 1)
                                        ->with('details.materialPrice')
                                        ->get();

            //List Semua bahan yang udah diterima
            $acceptedMaterials = $purchaseAcceptedGroup->map(function($purchase){
                return $purchase->details;
            })->collapse()->map(function($detail){
                return $detail->materialPrice;
            });

            foreach ($purchase->productions as $production) {
                // Cek kebutuhan material per produksi
                // Cek material sudah diterima atau belum
                $materialNeeded = $production->production->details->map(function($detail){
                    return $detail->productVariant->product->boms;
                })->collapse()->map(function($material){
                    return $material->material_id;
                })->unique()->values();

                $countAcceptedMaterial = $acceptedMaterials->whereIn('material_id', $materialNeeded->toArray())->count();

                if($materialNeeded->count() == $countAcceptedMaterial){
                    $production->production->status = 2;
                    $production->production->save();
                }
            }
            \DB::commit();
            return redirect()->back()->with('alert-success', 'Berhasil Menerima Pengadaan');
        } catch (\Exception $e) {
            \DB::rollback();
            return redirect()->back()->withInput()->with('alert-error', 'Terjadi kesalahan server');
        }
    }

    public function report(Request $request)
    {
        if($request->daterange){
            $daterange=explode(" - ", $request->daterange);
            $date_from = Carbon::createFromFormat('d/m/Y', $daterange[0]);
            $date_to = Carbon::createFromFormat('d/m/Y', $daterange[1]);

            $from = $date_from->format('Y-m-d');
            $to = $date_to->format('Y-m-d');
        } else{
            $from = date("Y-m-d", strtotime('-1 months'));
            $to = date("Y-m-d");

            $date_from = Carbon::createFromFormat('Y-m-d', $from);
            $date_to = Carbon::createFromFormat('Y-m-d', $to);
        }

        $reports = Purchase::whereDate('created_at','>=', $from)
                    ->whereDate('created_at','<=', $to)
                    ->get();

        $filename = 'Report Purchase '.$date_from->isoFormat('D MMMM Y').' - '.$date_to->isoFormat('D MMMM Y');
        return Excel::download(new PurchaseReport($reports), $filename.'.csv');

        return view('report.purchase', compact('reports'));
    }

    public function printDetail(Purchase $purchase)
    {
        $purchase->load('supplier', 'details.materialPrice.material');
        $code = str_replace('/', '-', $purchase->code);
        $filename = 'Report Purchase '.$code;

        $data=[
            'purchase'=>$purchase
        ];

        $pdf = PDF::loadView('report.purchase_detail', $data);
        // return $pdf->stream();
        return $pdf->download($filename.'.pdf');
    }
}
