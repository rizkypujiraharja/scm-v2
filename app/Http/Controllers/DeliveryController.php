<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Delivery, Car, Order};
use Carbon\Carbon;
use Excel;
use PDF;
use App\Exports\DeliveryReport;

use App\Mail\DeliveryOrder;
use Illuminate\Support\Facades\Mail;

class DeliveryController extends Controller
{
    public function index(Request $request)
    {
        $deliveries = Delivery::latest();

        if($request->daterange){
            $daterange=explode(" - ", $request->daterange);
            $date_from = Carbon::createFromFormat('d/m/Y', $daterange[0]);
            $date_to = Carbon::createFromFormat('d/m/Y', $daterange[1]);

            $from = $date_from->format('Y-m-d');
            $to = $date_to->format('Y-m-d');
        } else {
            $from = date("Y-m-d", strtotime('-1 months'));
            $to = date("Y-m-d");
        }

        $deliveries->whereDate('created_at','>=', $from)
                ->whereDate('created_at','<=', $to);

        $deliveries = $deliveries->paginate(config('app.per_page'));

        $notifDelivery = Order::where('status', 2)->count();

        return view('delivery.index', compact('deliveries', 'notifDelivery'));
    }

    public function create(Request $request)
    {
        $cars = Car::get();
        $orders = Order::where('status', 2)->get();

        return view('delivery.create', compact('cars', 'orders'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'car' => 'required',
            'order_ids' => 'required|array|min:1'
        ]);

        try {
            \DB::beginTransaction();
            $delivery = new Delivery;
            $delivery->car_id = $request->car;
            $delivery->code = $delivery->generateCode();
            $delivery->save();

            $orderIds = collect($request->order_ids)->map(function($id){
                return ['order_id' => $id];
            });

            Order::whereIn('id', $request->order_ids)->update(['status' => 3]);

            $delivery->details()->createMany($orderIds);

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            return redirect()->back()->withInput()->with('alert-error', 'Terjadi kesalahan server');
        }

        try {
            $delivery->with('details.order.details');
            foreach ($delivery->details as $key => $value) {
                $order = $value->order;
                $products = $order->details->groupBy('product_name');

                Mail::to($order->customer_email)->send(new DeliveryOrder($order, $products, $delivery->tanggal_pengiriman));
            }
        } catch (\Exception $e) {

        }

        return redirect()->route('deliveries.index')->with('alert-success', 'Berhasil Menambah Pengiriman');
    }

    public function show(Delivery $delivery)
    {
        $delivery->load('details.order', 'car');

        return view('delivery.show', compact('delivery'));
    }

    public function report(Request $request)
    {
        if($request->daterange){
            $daterange=explode(" - ", $request->daterange);
            $date_from = Carbon::createFromFormat('d/m/Y', $daterange[0]);
            $date_to = Carbon::createFromFormat('d/m/Y', $daterange[1]);

            $from = $date_from->format('Y-m-d');
            $to = $date_to->format('Y-m-d');
        } else{
            $from = date("Y-m-d", strtotime('-1 months'));
            $to = date("Y-m-d");

            $date_from = Carbon::createFromFormat('Y-m-d', $from);
            $date_to = Carbon::createFromFormat('Y-m-d', $to);
        }

        $reports = Delivery::whereDate('created_at','>=', $from)
                    ->whereDate('created_at','<=', $to)
                    ->with('details.order', 'car')
                    ->get();

        $filename = 'Report Pengiriman '.$date_from->isoFormat('D MMMM Y').' - '.$date_to->isoFormat('D MMMM Y');
        return Excel::download(new DeliveryReport($reports), $filename.'.csv');

        // return view('report.delivery', compact('reports'));
    }

    public function printDetail(Delivery $delivery)
    {
        $delivery->load('details.order', 'car');
        $code = str_replace('/', '-', $delivery->code);

        $filename = 'Report Pengiriman '.$code;
        $pdf = PDF::loadView('report.delivery_detail', compact('delivery'));
        return $pdf->download($filename.'.pdf');
    }

    public function destroy(Delivery $delivery)
    {
        $delivery->load(['details' => function($q){ $q->with('order'); }]);
        foreach ($delivery->details as $detail) {
            $detail->order->status = 2;
            $detail->order->save();
        }

        $delivery->delete();
        return redirect()->back()->with('alert-success', 'Berhasil menghapus pesanan');
    }
}
