<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Order, Product, Production, Customer, ProductVariant};
use App\Exports\{OrderReport, OrderDetailReport};

use Carbon\Carbon;
use Excel;
use PDF;

use App\Mail\InvoiceOrder;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
	public function index(Request $request)
    {
        $orders = Order::latest();

        if($request->has('search')){
            $query = '%'.$request->search.'%';
            $orders->where('customer_name', 'LIKE', $query);
        }

        if(\Auth::user()->role == 'bag_produksi'){
            $orders->where('status', 0);
        }

        if($request->daterange){
            $daterange=explode(" - ", $request->daterange);
            $date_from = Carbon::createFromFormat('d/m/Y', $daterange[0]);
            $date_to = Carbon::createFromFormat('d/m/Y', $daterange[1]);

            $from = $date_from->format('Y-m-d');
            $to = $date_to->format('Y-m-d');
        } else {
            $from = date("Y-m-d", strtotime('-1 months'));
            $to = date("Y-m-d");
        }

        $orders->whereDate('created_at','>=', $from)
                ->whereDate('created_at','<=', $to);

        $orders = $orders->paginate(config('app.per_page'));

        return view('order.index', compact('orders'));
    }

    public function create(Request $request)
    {
        $products = Product::with('variants')->orderBy('code')->get();
        $customers = Customer::orderBy('name')->get();
        $orders = Order::with(['details'])->where('status', '<', 2)->get();
        $totalQueueProductions = (int) $orders->map(function ($order){
            $order->total_size = $order->details->map(function($detail){
                $detail->total_size = $detail->product_size * $detail->quantity;
                return $detail;
            })->sum('total_size');
            return $order;
        })->sum('total_size');

    	return view('order.create', compact('products','customers','totalQueueProductions'));
    }

    public function show(Order $order)
    {
        $order->with('details');
        $products = $order->details->groupBy('product_name');

        return view('order.show', compact('order', 'products'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'order' => ['required','array','min:1',
                function ($attribute, $value, $fail) {
                    $cek = collect($value)->filter(function ($item, $key) {
                        return $item['quantity'] > 0;
                    })->count();
                    if (!$cek) {
                        $fail('pesanan produk wajib ada');
                    }
                }
            ],
            'type' => 'required|in:offline,marketplace'
        ],[],[
            'order' => 'pesanan produk',
            'type' => 'tipe pesanan',
        ]);

        try {
            \DB::beginTransaction();

            $orderProducts = collect($request->order)->filter(function ($item, $key) {
                return $item['quantity'] > 0;
            })->values()->map(function($product){
                if($product['stock']){
                    if($product['stock'] > $product['quantity']){
                        $product['from_stock'] = $product['quantity'];
                    }else{
                        $product['from_stock'] = $product['stock'];
                    }
                }else{
                    $product['from_stock'] = 0;
                }
                unset($product['stock']);
                return $product;
            });

            foreach ($orderProducts as $product) {
                $prod = ProductVariant::find($product['product_variant_id'])->decrement('stock', $product['from_stock']);
            }

            $totalPrice = $orderProducts->map(function($value){
                return $value['quantity'] * $value['price'];
            })->sum();

            if($request->customer_id){
                $customer =  Customer::find($request->customer_id);
            } else {
                $customer = new Customer;
                $customer->code = $customer->generateCode();
            }

            $customer->name = $request->customer_name;
            $customer->phone = $request->customer_phone;
            $customer->email = $request->customer_email;
            $customer->address = $request->customer_address;
            $customer->save();

            $status = 0;
            $needProduction = $orderProducts->filter(function($product){
                return $product['from_stock'] != $product['quantity'];
            });
            if(!$needProduction->count()){
                $status = 2;
            }

            $order = new Order;
            $order->invoice_code = $order->generateCode();
            $order->customer_id = $customer->id;
            $order->customer_name = $request->customer_name;
            $order->customer_phone = $request->customer_phone;
            $order->customer_email = $request->customer_email;
            $order->customer_address = $request->customer_address;
            $order->type = $request->type;
            $order->estimated_days = $order->getEstimatedDay($orderProducts);
            $order->total_price = $totalPrice;
            $order->status = $status;
            $order->save();

            $order->details()->createMany($orderProducts);

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            return redirect()->back()->withInput()->with('alert-error', 'Terjadi kesalahan server');
        }

        try {
            $order->with('details');
            $products = $order->details->groupBy('product_name');

            Mail::to($order->customer_email)->send(new InvoiceOrder($order, $products));
        } catch (\Exception $e) {

        }

        return redirect()->route('orders.index')->with('alert-success', 'Berhasil Menambah Pesanan');

    }

    public function toggleStatus(Order $order)
    {
        try {
            \DB::beginTransaction();

            $order->status = 1;
            $order->save();

            $productions = $order->details->map(function($detail){
                $detail->sub_total_size = $detail->quantity * $detail->product_size;
                return $detail->only('product_name', 'product_variant_id', 'quantity', 'product_size', 'sub_total_size');
            });
            $totalSize = $productions->sum('sub_total_size');

            $production = new Production;
            $production->code = $production->generateCode();
            $production->type = 'order';
            $production->order_id = $order->id;
            $production->status = 0;
            $production->total_size = $totalSize;
            $production->save();
            $production->details()->createMany($productions->toArray());

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            return redirect()->back()->withInput()->with('alert-error', 'Terjadi kesalahan server');
        }

        return redirect()->back()->with('alert-success', 'Berhasil Menambah Data Produksi');
    }

    public function report(Request $request)
    {
        if($request->daterange){
            $daterange=explode(" - ", $request->daterange);
            $date_from = Carbon::createFromFormat('d/m/Y', $daterange[0]);
            $date_to = Carbon::createFromFormat('d/m/Y', $daterange[1]);

            $from = $date_from->format('Y-m-d');
            $to = $date_to->format('Y-m-d');
        } else{
            $from = date("Y-m-d", strtotime('-1 months'));
            $to = date("Y-m-d");

            $date_from = Carbon::createFromFormat('Y-m-d', $from);
            $date_to = Carbon::createFromFormat('Y-m-d', $to);
        }

        $reports = Order::whereDate('created_at','>=', $from)
                    ->whereDate('created_at','<=', $to)
                    ->get();

        $filename = 'Report Order '.$date_from->isoFormat('D MMMM Y').' - '.$date_to->isoFormat('D MMMM Y');
        return Excel::download(new OrderReport($reports), $filename.'.csv');
    }

    public function printDetail(Order $order)
    {
        $order->with('details');
        $products = $order->details->groupBy('product_name');
        $code = str_replace('/', '-', $order->invoice_code);

        $filename = 'Report Order '.$code;

        $data = [
            'order'=>$order,
            'products'=>$products,
        ];

        $pdf = PDF::loadView('report.order_detail', $data);
        return $pdf->download($filename.'.pdf');
    }

    public function destroy(Order $order)
    {
        if($order->status == 2){
            $order->load(['details' => function($q){ $q->with('productVariant'); }]);
            foreach ($order->details as $detail) {
                $detail->productVariant->stock += $detail->quantity;
                $detail->productVariant->save();
            }
        }

        $order->delete();
        return redirect()->back()->with('alert-success', 'Berhasil menghapus pesanan');
    }

}
