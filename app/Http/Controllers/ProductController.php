<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Product, Material, BomProduct, ProductVariant};

use Carbon\Carbon;

use Excel;
use App\Exports\{StockProductReport};

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::latest();

        if($request->has('search')){
            $query = '%'.$request->search.'%';
            $products->where('name', 'LIKE', $query);
        }

        $products = $products->paginate(config('app.per_page'));

        return view('product.index', compact('products'));
    }

    public function create()
    {
        $materials = Material::select('id', 'name', 'unit')->get()->append('unit_string');
        return view('product.create', compact('materials'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|min:5',
            'type' => 'required|in:sabun,parfum'
        ],[],[
            'name' => 'nama produk',
            'type' => 'jenis produk'
        ]);

        try {
            \DB::beginTransaction();
            $product = new Product;
            $product->name = $request->name;
            $product->code = $product->generateCode($request->type);
            $product->type = $request->type;
            $product->save();

            $product->boms()->createMany($request->boms);
            $product->variants()->createMany($request->variants);

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            return redirect()->back()->withInput()->with('alert-error', 'Terjadi kesalahan server');
        }

        return redirect()->route('products.index')->with('alert-success', 'Berhasil Menambah Produk');
    }

    public function show(Product $product)
    {
        $product->load('variants', 'boms.material');
        return view('product.show', compact('product'));
    }

    public function edit(Product $product)
    {
        $product->load('variants', 'boms.material');
        $materials = Material::select('id', 'name', 'unit')->get();

        return view('product.edit', compact('product', 'materials'));
    }

    public function update(Request $request, Product $product)
    {
        $this->validate($request, [
            'name' => 'required|string|min:5',
            'type' => 'required|in:sabun,parfum'
        ],[],[
            'name' => 'nama produk',
            'type' => 'jenis produk'
        ]);
        try {
            \DB::beginTransaction();
            $product->name = $request->name;
            if($product->type != $request->type){
                $product->code = $product->generateCode($request->type);
            }
            $product->type = $request->type;
            $product->save();

            foreach ($request->boms as $key => $bom) {
                BomProduct::updateOrCreate(
                    ['product_id' => $product->id, 'material_id' => $bom['material_id']],
                    ['dose' => $bom['dose']]
                );
            }

            foreach ($request->variants as $key => $variant) {
                ProductVariant::updateOrCreate(
                    ['product_id' => $product->id, 'size' => $variant['size']],
                    ['price' => $variant['price'], 'stock' => $variant['stock']]
                );
            }

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            return redirect()->back()->withInput()->with('alert-error', 'Terjadi kesalahan server');
        }

        return redirect()->route('products.show', $product)->with('alert-success', 'Berhasil Mengubah Produk');
    }

    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('products.index')->with('alert-success', 'Berhasil menghapus produk');
    }

    public function report(Request $request)
    {
        $date = Carbon::now()->isoFormat('D MMMM Y');

        $reports = Product::with('variants')
                    ->get();

        $filename = 'Report Stock Product '.$date;
        return Excel::download(new StockProductReport($reports), $filename.'.csv');

        return view('report.stock_product', compact('reports'));
    }
}
