<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Image;
use Storage;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::latest();

        if($request->has('search')){
            $query = '%'.$request->search.'%';
            $users->where('name', 'LIKE', $query);
        }

        $users = $users->paginate(config('app.per_page'));

        return view('users.index', compact('users'));
    }

    public function create()
    {
        return view('users.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nip' => 'required|numeric|unique:users,nip',
            'name' => 'required|min:4|max:50|string',
            'address' => 'nullable|string|max:200',
            'phone' => 'nullable',
            'photo' => 'nullable|image',
            'role' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|string|min:6',
        ], [], [
            'name' => 'nama',
            'address' => 'alamat',
            'photo' => 'foto'
        ]);

        $user = new User;

        if( !is_null($request->photo) ){
            $file = $request->file('photo');
            $path = $file->hashName('photos');

            $image = Image::make($file);
            $image->fit(300, 300, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put($path, (string) $image->encode());
            $user->photo = $path;
        }

        $user->nip = $request->nip;
        $user->name = $request->name;
        $user->address = $request->address;
        $user->phone = $request->phone;
        $user->role = $request->role;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        return redirect()->route('users.index')->with('alert-success', 'Berhasil menambah pegawai!');
    }

    public function show(User $user)
    {
        return view('users.show', compact('user'));
    }

    public function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'nip' => 'required|numeric|unique:users,nip,'.$user->id,
            'name' => 'required|min:4|max:50|string',
            'address' => 'nullable|string|max:200',
            'phone' => 'nullable',
            'photo' => 'nullable|image',
            'role' => 'required',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'password' => 'nullable|string|min:6',
        ], [], [
            'name' => 'nama',
            'address' => 'alamat',
            'photo' => 'foto'
        ]);

        if( !is_null($request->photo) ){
            $file = $request->file('photo');
            $path = $file->hashName('photos');

            $image = Image::make($file);
            $image->fit(300, 300, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::put($path, (string) $image->encode());
            $user->photo = $path;
        }

        $user->nip = $request->nip;
        $user->name = $request->name;
        $user->address = $request->address;
        $user->phone = $request->phone;
        $user->role = $request->role;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        return redirect()->route('users.index')->with('alert-success', 'Berhasil mengubah pegawai!');
    }

    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('users.index')->with('alert-success', 'Berhasil menghapus data pegawai');
    }
}
