<?php

namespace App\Http\Controllers;

use App\Models\{Supplier, Material, MaterialPrice};
use Illuminate\Http\Request;

class MaterialPriceController extends Controller
{
    public function index(Supplier $supplier)
    {
        $supplier->load('materials.material');
        $materials = $supplier->materials()->with('material')->paginate(config('app.per_page'));
        return view('supplier.material.index', compact('supplier', 'materials'));
    }

    public function create(Supplier $supplier)
    {
        $supplier->load('materials');
        $supplierMaterialIds = $supplier->materials->pluck('material_id');
        $materials = Material::whereNotIn('id', $supplierMaterialIds)->get();

        return view('supplier.material.create', compact('supplier', 'materials'));
    }

    public function store(Supplier $supplier, Request $request)
    {
        $this->validate($request, [
            'material_id' => 'required|exists:material_prices,id',
            'price' => 'required',
            'min_order' => 'required'
        ], [], [
            'material_id' => 'bahan baku',
            'price' => 'harga',
            'min_order' => 'minimal pembelian'
        ]);

        $materialPrice = new MaterialPrice;
        $materialPrice->supplier_id = $supplier->id;
        $materialPrice->material_id = $request->material;
        $materialPrice->price = $request->price;
        $materialPrice->min_order = $request->min_order;
        $materialPrice->save();

        return redirect()->route('suppliers.materials.index', $supplier)->with('alert-success', 'Berhasil menambah material');
    }

    public function edit(Supplier $supplier, MaterialPrice $material)
    {
        $this->check($supplier, $material);
        $material->load('material');
        return view('supplier.material.edit', compact('supplier', 'material'));
    }

    public function update(Supplier $supplier, MaterialPrice $material, Request $request)
    {
        $this->check($supplier, $material);

        $this->validate($request, [
            'material_id' => 'required|exists:material_prices,id',
            'price' => 'required',
            'min_order' => 'required'
        ], [], [
            'material_id' => 'bahan baku',
            'price' => 'harga',
            'min_order' => 'minimal pembelian'
        ]);

        $material->price = $request->price;
        $material->min_order = $request->min_order;
        $material->save();

        return redirect()->route('suppliers.materials.index', $supplier)->with('alert-success', 'Berhasil mengubah material');
    }

    public function destroy(Supplier $supplier, MaterialPrice $material)
    {
        $this->check($supplier, $material);
        $material->delete();

        return redirect()->route('suppliers.materials.index', $supplier)->with('alert-success', 'Berhasil menghapus material');
    }

    private function check($supplier, $material){
        if($material->supplier_id != $supplier->id){
            abort(404);
        }
        return;
    }
}
