<?php

namespace App\Http\Controllers;

use App\Models\Material;
use Illuminate\Http\Request;

use Carbon\Carbon;

use Excel;
use App\Exports\{StockMaterialReport};

class MaterialController extends Controller
{
    public function index(Request $request)
    {
        $materials = Material::latest();

        if($request->has('search')){
            $query = '%'.$request->search.'%';
            $materials->where('name', 'LIKE', $query);
        }

        $materials = $materials->paginate(config('app.per_page'));

        return view('material.index', compact('materials'));
    }

    public function create()
    {
        return view('material.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|min:3',
            'unit' => 'required|string|in:l,g',
            'stock' => 'required|numeric'
        ], [], [
            'name' => 'nama bahan baku',
            'unit' => 'satuan'
        ]);

        $material = new Material;
        $material->name = $request->name;
        $material->code = $material->generateCode($request->name);
        $material->unit = $request->unit;
        $material->stock = $request->stock;
        $material->save();

        return redirect()->route('materials.index')->with('alert-success', 'Berhasil menambah data bahan baku');
    }

    public function show(Material $material)
    {
        return view('material.edit', compact('material'));
    }

    public function edit(Material $material)
    {
        return view('material.edit', compact('material'));
    }

    public function update(Request $request, Material $material)
    {
        $this->validate($request, [
            'name' => 'required|string|min:3',
            'unit' => 'required|string|in:l,g',
        ], [], [
            'name' => 'nama bahan baku',
            'unit' => 'satuan'
        ]);

        $material->name = $request->name;
        $material->unit = $request->unit;
        $material->save();

        return redirect()->route('materials.index')->with('alert-success', 'Berhasil mengubah data bahan baku');
    }

    public function destroy(Material $material)
    {
        $material->delete();
        return redirect()->route('materials.index')->with('alert-success', 'Berhasil menghapus data bahan baku');
    }

    public function report(Request $request)
    {
        $date = Carbon::now()->isoFormat('D MMMM Y');

        $reports = Material::get();

        $filename = 'Report Stock Material '.$date;
        return Excel::download(new StockMaterialReport($reports), $filename.'.csv');

        return view('report.stock_material', compact('reports'));
    }
}
