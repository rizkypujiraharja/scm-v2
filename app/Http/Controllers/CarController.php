<?php

namespace App\Http\Controllers;

use App\Models\Car;
use Illuminate\Http\Request;

class CarController extends Controller
{
    public function index(Request $request)
    {
        $cars = Car::latest();

        $cars = $cars->paginate(config('app.per_page'));

        return view('car.index', compact('cars'));
    }

    public function create()
    {
        return view('car.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'no_pol' => 'required|string|min:3',
            'merk' => 'required|string',
        ]);

        $car = new Car;
        $car->no_pol = $request->no_pol;
        $car->merk = $request->merk;
        $car->save();

        return redirect()->route('cars.index')->with('alert-success', 'Berhasil menambah data mobil');
    }

    public function show(Car $car)
    {
        return view('car.edit', compact('car'));
    }

    public function edit(Car $car)
    {
        return view('car.edit', compact('car'));
    }

    public function update(Request $request, Car $car)
    {
        $this->validate($request, [
            'no_pol' => 'required|string|min:3',
            'merk' => 'required|string',
        ], [], [
            'merk' => 'merek'
        ]);

        $car->no_pol = $request->no_pol;
        $car->merk = $request->merk;
        $car->save();

        return redirect()->route('cars.index')->with('alert-success', 'Berhasil mengubah data mobil');
    }

    public function destroy(Car $car)
    {
        $car->delete();
        return redirect()->route('cars.index')->with('alert-success', 'Berhasil menghapus data mobil');
    }
}
