<?php

namespace App\Http\Controllers;

use App\Models\Supplier;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    public function index(Request $request)
    {
        $suppliers = Supplier::latest();

        if($request->has('search')){
            $query = '%'.$request->search.'%';
            $suppliers->where('name', 'LIKE', $query);
        }

        $suppliers = $suppliers->paginate(config('app.per_page'));

        return view('supplier.index', compact('suppliers'));
    }

    public function create()
    {
        return view('supplier.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|min:3',
            'address' => 'required|string|min:10',
            'phone' => 'required|string|numeric'
        ], [], [
            'name' => 'nama supplier',
            'address' => 'alamat',
            'phone' => 'nomor telepon'
        ]);

        $supplier = new Supplier;
        $supplier->name = $request->name;
        $supplier->code = $supplier->generateCode();
        $supplier->address = $request->address;
        $supplier->phone = $request->phone;
        $supplier->save();

        return redirect()->route('suppliers.index')->with('alert-success', 'Berhasil menambah data supplier');
    }

    public function show(Supplier $supplier)
    {
        return view('supplier.edit', compact('supplier'));
    }

    public function edit(Supplier $supplier)
    {
        return view('supplier.edit', compact('supplier'));
    }

    public function update(Request $request, Supplier $supplier)
    {
        $this->validate($request, [
            'name' => 'required|string|min:3',
            'address' => 'required|string|min:10',
            'phone' => 'required|string|numeric'
        ], [], [
            'name' => 'nama supplier',
            'address' => 'alamat',
            'phone' => 'nomor telepon'
        ]);

        $supplier->name = $request->name;
        $supplier->address = $request->address;
        $supplier->phone = $request->phone;
        $supplier->save();

        return redirect()->route('suppliers.index')->with('alert-success', 'Berhasil mengubah data supplier');
    }

    public function destroy(Supplier $supplier)
    {
        $supplier->delete();
        return redirect()->route('suppliers.index')->with('alert-success', 'Berhasil menghapus data supplier');
    }
}
