<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\{Supplier,Order,Product,Purchase,Production,Customer};
use Carbon\{Carbon, CarbonPeriod};


use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    
    public function widget(Request $request)
    {
        $supplier = Supplier::count();
        $product = Product::count();
        $customer = Customer::count();
        $orders = Order::get()->groupBy('status')
                    ->map(function ($row) {
                        return $row->count();
                    });
        $status=[
            (object)[
                "code"=>"1",
                "label"=>"produksi"
            ],
            (object)[
                "code"=>"2",
                "label"=>"siap kirim"
            ],
            (object)[
                "code"=>"3",
                "label"=>"dikirim"
            ],
        ];

        $order=[];
        $total_order=0;
        foreach ($status as $key => $value) {
            $total = $orders[$value->code] ?? 0;
            $value->total = $total;

            $order[]=$value;
            $total_order+=$total;
        }
        // dd($order, $total_order);
        $data=[
            'order'=>(object)[
                'total'=>$total_order,
                'data'=>$order
            ],
            'supplier'=>(object)[
                'total'=>$supplier,
            ],
            'product'=>(object)[
                'total'=>$product,
            ],
            'customer'=>(object)[
                'total'=>$customer,
            ],
        ];

        return response()->json($data);
    }

    public function chartOrder(Request $request)
    {
        $orders = Order::select('*', DB::raw('DATE(created_at) as date'))->get()->groupBy('date')
                    ->map(function ($row) {
                        return $row->count();
                    });

        if($orders->count()){
            $from = Order::first()->created_at->format('Y-m-d');
            $to = Order::latest()->first()->created_at->format('Y-m-d');
            
            $dateRanges = CarbonPeriod::create(Carbon::createFromFormat('Y-m-d', $from), Carbon::createFromFormat('Y-m-d', $to))->toArray();

            foreach ($dateRanges as $key => $date) {
                $categories[] = $date->format('d/m/y');
            }

            $all=0;

            foreach ($dateRanges as $date) {
                $indexDate = $date->format('Y-m-d');
                $total = $orders[$indexDate] ?? 0 ;

                $newdate = $date->format('d/m/y');
                $data[] = [
                    "name"=> $newdate,
                    "y"=>$total,
                ];

                $all+=$total;
            }

            $data_series[]=[
                "data"=>$data,
                "name"=>'Pesanan',
            ];

            $data = [
                "chart" => $data_series,
                "categories" =>$categories, 
                "total"=>$all
            ];
        } else {
             $data = [
                "chart" => [],
                "categories" =>[], 
                "total"=>0
            ];
        }
        

        // dd($orders, $data);
        return response()->json($data); 
    }

    public function chartProduction(Request $request)
    {
        $production = Production::select('*', DB::raw('DATE(created_at) as date'))->get()->groupBy('date')
                    ->map(function ($row) {
                        return $row->sum('total_size');
                    });

        if($production->count()){
            $from = Production::first()->created_at->format('Y-m-d');
            $to = Production::latest()->first()->created_at->format('Y-m-d');
            
            $dateRanges = CarbonPeriod::create(Carbon::createFromFormat('Y-m-d', $from), Carbon::createFromFormat('Y-m-d', $to))->toArray();

            foreach ($dateRanges as $key => $date) {
                $categories[] = $date->format('d/m/y');
            }

            $all=0;

            foreach ($dateRanges as $date) {
                $indexDate = $date->format('Y-m-d');
                $total = $production[$indexDate] ?? 0 ;

                $newdate = $date->format('d/m/y');
                $data[] = [
                    "name"=> $newdate,
                    "y"=>$total,
                ];

                $all+=$total;
            }

            $data_series[]=[
                "data"=>$data,
                "name"=>'Produksi (Liter/hari)',
            ];

            $data = [
                "chart" => $data_series,
                "categories" =>$categories, 
                "total"=>$all
            ];
        } else {
            $data = [
                "chart" => [],
                "categories" =>[], 
                "total"=>0
            ];
        }
        

        

        // dd($production, $data);
        return response()->json($data); 
    }

    public function recentOrder(Request $request)
    {
        $orders = Order::latest()->limit(5)->get();

        return response()->json($orders); 
    }

    public function recentPurchase(Request $request)
    {
        $purchases = Purchase::latest()->limit(5)->get();

        return response()->json($purchases); 
    }

    public function recentProduction(Request $request)
    {
        $productions = Production::latest()->limit(5)->get();

        return response()->json($productions); 
    }

}
