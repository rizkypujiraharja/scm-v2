<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Production, Product, Order};

use Carbon\Carbon;
use Excel;
use App\Exports\{ProductionReport};

use PDF;

class ProductionController extends Controller
{
    public function index(Request $request)
    {
        $productions = Production::latest();

        if($request->daterange){
            $daterange=explode(" - ", $request->daterange);
            $date_from = Carbon::createFromFormat('d/m/Y', $daterange[0]);
            $date_to = Carbon::createFromFormat('d/m/Y', $daterange[1]);

            $from = $date_from->format('Y-m-d');
            $to = $date_to->format('Y-m-d');
        } else {
            $from = date("Y-m-d", strtotime('-1 months'));
            $to = date("Y-m-d");
        }

        $productions->whereDate('created_at','>=', $from)
                ->whereDate('created_at','<=', $to);

        $productions = $productions->paginate(config('app.per_page'));

        return view('production.index', compact('productions'));
    }

    public function create()
    {
        $products = Product::with('variants')->orderBy('code')->get();
    	return view('production.create', compact('products'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'production' => ['required','array','min:1',
                function ($attribute, $value, $fail) {
                    $cek = collect($value)->filter(function ($item, $key) {
                            return $item['quantity'] > 0;
                        })->count();
                        if (!$cek) {
                            $fail('jumlah produksi wajib ada');
                        }
                }
            ]
        ], [], [
            'production' => 'data produksi'
        ]);

        try {
            $productions = collect($request->production)->filter(function ($item, $key) {
                return $item['quantity'] > 0;
            })->values();
            $totalSize = $productions->map(function($value){
                return $value['quantity'] * $value['product_size'];
            })->sum();

            \DB::beginTransaction();
            $production = new Production;
            $production->code = $production->generateCode();
            $production->type = 'stock';
            $production->status = 0;
            $production->total_size = $totalSize;
            $production->save();

            $production->details()->createMany($productions->toArray());

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            dd($e);
            return redirect()->back()->withInput()->with('alert-error', 'Terjadi kesalahan server');
        }

        return redirect()->route('productions.index')->with('alert-success', 'Berhasil Menambah Data Produksi');
    }

    public function show(Production $production)
    {
        $production->with('details');
        $products = $production->details->groupBy('product_name');
        return view('production.show', compact('production', 'products'));
    }

    public function toggleStatus(Request $request, Production $production)
    {
        try {
            $production->status = $request->status;
            $production->save();

            if($request->status == 4){
                if($production->type == 'stock'){
                    $production->load(['details' => function($q){ $q->with('productVariant'); }]);
                    foreach ($production->details as $detail) {
                        $detail->productVariant->stock += $detail->quantity;
                        $detail->productVariant->save();
                    }
                }else{
                    $production->load('order');

                    $order = $production->order;
                    $order->status = 2;
                    $order->save();
                }
            }

            return redirect()->back()->with('alert-success', 'Berhasil Mengubah Status Produksi');
        } catch (\Exception $e) {
            \DB::rollback();
            return redirect()->back()->withInput()->with('alert-error', 'Terjadi kesalahan server');
        }
    }

    public function report(Request $request)
    {
        if($request->daterange){
            $daterange=explode(" - ", $request->daterange);
            $date_from = Carbon::createFromFormat('d/m/Y', $daterange[0]);
            $date_to = Carbon::createFromFormat('d/m/Y', $daterange[1]);

            $from = $date_from->format('Y-m-d');
            $to = $date_to->format('Y-m-d');
        } else{
            $from = date("Y-m-d", strtotime('-1 months'));
            $to = date("Y-m-d");

            $date_from = Carbon::createFromFormat('Y-m-d', $from);
            $date_to = Carbon::createFromFormat('Y-m-d', $to);
        }

        $reports = Production::whereDate('created_at','>=', $from)
                    ->whereDate('created_at','<=', $to)
                    ->get();

        $filename = 'Report Production '.$date_from->isoFormat('D MMMM Y').' - '.$date_to->isoFormat('D MMMM Y');
        return Excel::download(new ProductionReport($reports), $filename.'.csv');

        return view('report.production', compact('reports'));
    }

    public function printDetail(Production $production)
    {
        $production->with('details');
        $products = $production->details->groupBy('product_name');
        $code = str_replace('/', '-', $production->code);

        $filename = 'Report Production '.$code;

        $data = [
            'production'=>$production,
            'products'=>$products,
        ];

        $pdf = PDF::loadView('report.production_detail', $data);
        // return $pdf->stream();
        return $pdf->download($filename.'.pdf');
    }
}
