<?php

use Illuminate\Database\Seeder;
use App\Models\{Order, Product};

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create('id_ID');
        $products = Product::with('variants')->inRandomOrder()->limit(rand(2, 5))->get();

        $orderProducts = [];

        foreach ($products as $product) {
            $variants = $product->variants()->inRandomOrder()->limit(rand(2,4))->get();
            foreach ($variants as $variant) {
                $quantity = rand(10, 100);
                $orderProducts[] = [
                    'product_variant_id' => $variant->id,
                    'product_name' => $product->name,
                    'product_size' => $variant->size,
                    'quantity' => $quantity,
                    'price' => $variant->price,
                    'sub_total_price' => $variant->price * $quantity
                ];
            }
        }

        $order = new Order;
        $order->invoice_code = $order->generateCode();
        $order->customer_name = $faker->name;
        $order->customer_phone = '085'.rand(111111111, 999999999);
        $order->customer_email = $faker->email;
        $order->customer_address = $faker->address;
        $order->type = rand(0,1) ? 'offline' : 'marketplace';
        $order->estimated_days = $order->getEstimatedDay($orderProducts);
        $order->created_at = now()->subDays(rand(1, 7));
        $order->updated_at = $order->created_at;
        $order->total_price = collect($orderProducts)->sum('sub_total_price');
        $order->save();

        $order->details()->createMany($orderProducts);
    }
}
