<?php

use Illuminate\Database\Seeder;
use App\Models\Supplier;

class SupplierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $suppliers = [
            'Central Kimia', 'Lautan Luas', 'Mulya Adhi', 'Bahtera Kimia', 'Kimia Jaya', 'Sumber Chemical'
        ];

        $faker = Faker\Factory::create('id_ID');
        foreach ($suppliers as $name) {
            $supplier = new Supplier;
            $supplier->name = $name;
            $supplier->code = $supplier->generateCode();
            $supplier->address = $faker->address;
            $supplier->phone = '085'.rand(111111111, 999999999);
            $supplier->save();
            sleep(1);
        }
    }
}
