<?php

use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $soaps = [
            'Deterjen antiseptic',
            'Deterjen extra wangi',
            'Deterjen Low Foam',
            'Deterjen extra',
            'Deterjen double extra',
            'Softerjen ',
            'Softener',
            'Pelicin',
            'Sabun Cuci Tangan',
            'Sabun Cuci Piring',
            'Sabun Pel Lantai',
        ];

        $parfums = [
            'Parfum Philis',
            'Parfum Apel',
            'Parfum Sweetheart',
            'Parfum Pink cherry',
            'Parfum Molto Pink',
            'Parfum Molto Black',
            'Parfum Selena',
            'Parfum Baby Lido',
            'Parfum Lido',
            'Parfum Molto Blue',
            'Parfum Downy Black',
            'Parfum Snappy',
            'Parfum Lovely',
            'Parfum Bubble Gum',
            'Parfum Akasia',
            'Parfum Sakura',
            'Parfum Daylight',
            'Parfum Idol',
            'Parfum Luxette',
            'Parfum Exotic',
            'Parfum Velvet Oris',
            'Parfum Sakurise',
            'Parfum Gardenia',
            'Parfum Vanilla',
            'Parfum Aigner',
            'Parfum Black Sweet',
            'Parfum Green Tea ',
            'Parfum Purple Grape',
            'Parfum True Girl',
            'Parfum Strawberry',
            'Parfum Skylight',
            'Parfum Lavender Bloom',
            'Parfum Ocean Fresh',
            'Parfum Dunhill',
            'Parfum Love Vine ',
            'Parfum Lily bloom',
            'Parfum Downy Passion',
            'Parfum Aqua Fresh',
            'Parfum Paris Hilton',
            'Parfum White Linen ',
            'Parfum Angel Heart ',
            'Parfum Lily ',
            'Parfum Lavender',
        ];

        $variants = [
            ['size' => 0.3, 'price' => 5000, 'stock' => 0],
            ['size' => 0.5, 'price' => 12000, 'stock' => 0],
            ['size' => 1, 'price' => 20000, 'stock' => 0],
            ['size' => 5, 'price' => 100000, 'stock' => 0]
        ];

        foreach ($soaps as $soap) {
            $product = new Product;
            $product->name = $soap;
            $product->code = $product->generateCode('soap');
            $product->type = 'sabun';
            $product->save();


            $boms = [
                ['material_id' => rand(1, 25), 'dose'=> 0.1],
                ['material_id' => rand(1, 25), 'dose'=> 0.2],
                ['material_id' => rand(1, 25), 'dose'=> 0.3],
            ];

            $product->boms()->createMany($boms);
            $product->variants()->createMany($variants);
            sleep(1);
        }

        foreach ($parfums as $parfum) {
            $product = new Product;
            $product->name = $parfum;
            $product->code = $product->generateCode('parfum');
            $product->type = 'parfum';
            $product->save();

            $product->boms()->createMany($boms);
            $product->variants()->createMany($variants);
            sleep(1);
        }
    }
}
