<?php

use Illuminate\Database\Seeder;
use App\Models\{Supplier, Material};

class MaterialPriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $suppliers = Supplier::get();
        $materials = Material::get();


        foreach ($suppliers as $supplier) {
            $materialPrices = [];
            foreach ($materials as $material) {
                $materialPrices[] = [
                    'material_id' => $material->id,
                    'price' => rand(5, 35) * 1000,
                    'min_order' => rand(3,15)
                ];
            }
            $supplier->materials()->createMany($materialPrices);
            usleep(50000);
        }
    }
}
