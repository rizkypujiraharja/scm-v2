<?php

use Illuminate\Database\Seeder;
use App\Models\Material;

class MaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $materials = [
            ['Texapon', 'l'],
            ['Sulfat', 'g'],
            ['Acid', 'g'],
            ['Soda Abu', 'g'],
            ['Det Booster', 'l'],
            ['NACL', 'g'],
            ['Edta N4', 'g'],
            ['Tamsis', 'l'],
            ['Oba', 'l'],
            ['Triclosan', 'l'],
            ['DMDM', 'l'],
            ['Magnasoft', 'l'],
            ['White Agen', 'l'],
            ['MICRO FRAGRANCE-400', 'l'],
            ['KATHONE', 'l'],
            ['Hysoft', 'l'],
            ['LABS', 'l'],
            ['LAS', 'l'],
            ['Ampitol', 'l'],
            ['BKC', 'l'],
            ['Glyserin Fg', 'l'],
            ['Betain', 'l'],
            ['Np-10', 'l'],
            ['PEG-40', 'l'],
            ['Methanol', 'l'],
        ];

        foreach ($materials as $data) {
            $material = new Material();
            $material->name = $data[0];
            $material->code = $material->generateCode($data[0]);
            $material->unit = $data[1];
            $material->stock = 0;
            $material->save();
            usleep(500000);
        }
    }
}
