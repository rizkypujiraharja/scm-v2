<?php

use Illuminate\Database\Seeder;
use App\Models\{Customer};

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('id_ID');

        $customer = new Customer;
        $customer->code = $customer->generateCode();
        $customer->name = $faker->name;
        $customer->phone = '085'.rand(111111111, 999999999);
        $customer->email = $faker->email;
        $customer->address = $faker->address;
        $customer->created_at = now()->subDays(rand(1, 7));
        $customer->updated_at = $customer->created_at;
        $customer->save();
    }
}
