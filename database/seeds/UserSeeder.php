<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->nip = '111111111111';
        $user->name = 'Ujang Syaefullah';
        $user->address = 'Limbangan';
        $user->phone = '085793517189';
        $user->photo = '';
        $user->email = 'ujang@scm.com';
        $user->password = bcrypt('secret');
        $user->role = 'administrator';
        $user->save();
    }
}
