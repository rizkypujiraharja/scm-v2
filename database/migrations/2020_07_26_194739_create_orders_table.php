<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('invoice_code', 100);
            $table->string('customer_name', 100);
            $table->string('customer_phone', 100);
            $table->string('customer_email', 100);
            $table->text('customer_address');
            $table->string('type', 25)->default('offlie');
            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('estimated_days');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
